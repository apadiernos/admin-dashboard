/* Webarch Admin Dashboard 
/* This JS is only for DEMO Purposes - Extract the code that you need
-----------------------------------------------------------------*/ 

$(document).ready(function() {	
	var base_url = $('body').attr("base_url");

	/**-------------------------------- Icon Validator Methods --------------------------------------------------------------**/
	/**
	 *  @params label object element for error message
	 *  @description errorplacement validation content
	 */
	$.fn.errorPlacementIcon = function(label){
		var icon = $(this).parent('.input-with-icon').children('i');
		if(label){
			if( $(this).attr('error-message') )
				label = $(this).attr('error-message');
			else 
				label = $(label).text();
			console.log(label);
			$(this).attr('id','popover').attr('data-toggle','popover').attr('data-placement','bottom').attr('data-content',label).popover('toggle');
		}
		var parent = $(this).parent('.input-with-icon');
		icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
		parent.removeClass('success-control').addClass('error-control');  				
	};
	/**
	 *  @description highlight validation content
	 */	
	$.fn.highlightIcon = function(){
		var parent = $(this).parent();
		parent.removeClass('success-control').addClass('error-control'); 				
	};
	/**
	 *  @description success validation content
	 */		
	$.fn.successIcon = function(){
		var icon = $(this).parent('.input-with-icon').children('i');
		var parent = $(this).parent('.input-with-icon');
		icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
		parent.removeClass('error-control').addClass('success-control'); 
		$(this).popover('destroy');		
	};		
	/**-------------------------------- Icon Validator Methods --------------------------------------------------------------**/
	
	/**-------------------------------- Address Ajax Loader Functions --------------------------------------------------------------**/
	/**
	 *  @description get municipalities using Address conroller. Located at address/Address.php
	 */		
	var getMunicpalitiesUrl = base_url+'address/address/getMunicpalities';
	$.fn.getMunicpalities = function(){
		var provCode = $(this).find(":selected").val();
		var el = $(this);
		$.ajax({
			type: 'POST',
			url: getMunicpalitiesUrl,
			data: {
				'provCode' : provCode,
			},
			success: function (municipalities) {
				if( municipalities ){
					el.parents('.location-row').find('.select2-container.city_municipality').remove();
					el.parents('.location-row').find("select.city_municipality").select2('destroy'); 
					var municipalitieshtm = "";
					var municipalities = $.parseJSON(municipalities);
					$.each(municipalities,function(key,municipality){
						municipalitieshtm += '<option value="'+municipality.citymunCode+'">'+municipality.citymunDesc+'</option>';
					});
					el.parents('.location-row').find('select.city_municipality').html(municipalitieshtm).select2();
				}
			}
		});	
	};	
	/**
	 *  @description get brgys using Address conroller. Located at address/Address.php
	 */		
	var getBrgysUrl = base_url+'address/address/getBrgys';
	$.fn.getBrgys = function(){
		var citymunCode = $(this).find(":selected").val();
		var el = $(this);
		$.ajax({
			type: 'POST',
			url: getBrgysUrl,
			data: {
				'citymunCode' : citymunCode,
			},
			success: function (brgys) {	
				if( brgys ){
					el.parents('.location-row').find('.select2-container.brgy_district').remove();
					el.parents('.location-row').find("select.brgy_district").select2('destroy'); 
					var brgyshtm = "";
					var brgys = $.parseJSON(brgys);
					$.each(brgys,function(key,brgy){
						brgyshtm += '<option value="'+brgy.brgyCode+'">'+brgy.brgyDesc+'</option>';
					});
					el.parents('.location-row').find('select.brgy_district').html(brgyshtm).select2();
				}
			}
		});	
	};		
	/**-------------------------------- Address Ajax Loader Functions --------------------------------------------------------------**/
	
   /**-------------------------------- Icon Validator Addt'l Methods --------------------------------------------------------------**/
	/**
	 *  @description check if record exists in database using Servervalidation Controller. Located at sadmin/Servervalidation.php
	 */	
	var recordvalidationUrl = base_url+'sadmin/servervalidation/recordexists';
	$.validator.addMethod("recordExists", function(value, element, param) {
		var table = $(element).attr('duplicate-table');
		var column = $(element).attr('duplicate-column');
		var model = $(element).attr('duplicate-model');
		var value = value;
		$.ajax({
			type: 'POST',
			url: recordvalidationUrl,
			data: {
				'table' : table,
				'model' : model,
				'column' : column,
				'value' : value,
			},
			success: function (res) {
				$(element).attr('res',res);
			}
		});
		
		if( $(element).attr('res')  == param )
			return false;
		else
			return true;
	},function(val,el){
		return $(el).attr('duplicate-message') ? $(el).attr('duplicate-message') : "Existing Database Record";
	});
	/**
	 *  @description check if value of .duplicate element has duplicates
	 */		
	$.validator.addMethod("valueExists", function(value, element, param) {
		var ctr = 0;
		var val = "";
		$(".duplicate").each(function(){
			if( val == $(this).val() ){
				ctr++;
			}
			val = $(this).val();			
		});
		if( ctr >= 2 )
			return false;
		else
			return true;
	},function(val){
		return "Existing Form Record";
	});
	/**-------------------------------- Icon Validator Addt'l Functions --------------------------------------------------------------**/    
	
	 /**-------------------------------- Icon Validator --------------------------------------------------------------**/
	$('.select2', "#form_traditional_validation").change(function () {
        $('#form_traditional_validation').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });
		
	//Iconic form validation sample	
   $('#form_iconic_validation').validate({
		errorElement: 'span', 
		errorClass: 'error', 
		focusInvalid: false, 

		ignore: "",
		//onkeyup: function(element) { $(element).popover('destroy'); },
		/**
		rules: {
			form1Name: {
				minlength: 2,
				required: true
			},
			email_address: {
				required: true,
				email: true
			},
			form1Url: {
				required: true,
				url: true
			},
			gendericonic:{
				required: true
			}
		},
		**/
		invalidHandler: function (event, validator) {
			//display error alert on form submit    
		},

		errorPlacement: function (label, element) { // render error placement for each input type
			$(element).errorPlacementIcon(label);
		},

		highlight: function (element) { // hightlight error inputs
			$(element).highlightIcon(); 
		},

		unhighlight: function (element) { // revert the change done by hightlight
			
		},

		success: function (label, element) {
			$(element).successIcon();
		},

		//submitHandler: function (form) {
		
		//}
       
	});
	$('.select2', "#form_iconic_validation").change(function () {
		$('#form_iconic_validation').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
	});
	$('body').on('blur','.duplicate',function () {
		$(this).rules('add', {
			recordExists: 1,
			valueExists: 1,
		});      
	});
/* 	$.validator.addClassRules('duplicate', {
		recordExists: 1,
		valueExists: 1,
	}); 	
 */	/**-------------------------------- Icon Validator --------------------------------------------------------------**/
		  
	/**-------------------------------- Address Ajax Loader --------------------------------------------------------------**/
	$('body').on('change','.province',function(){
		$(this).getMunicpalities();
		$(this).parents('.location-row').find('select.brgy_district').html('');
	});
	
	
	$('body').on('change','.city_municipality',function(){
		$(this).getBrgys();
	});
	$('.location-row').each(function(){
		$(this).find('.province').getMunicpalities();
		
		var city_mun_el = $(this).find('.city_municipality');
		var city_municipality = city_mun_el.attr('default');
		var brgy_district_el = $(this).find('.city_municipality');
		var brgy_district = brgy_district_el.attr('default');
		
		setTimeout(function(){
			city_mun_el.select2('destroy').val(pad("0" + city_municipality, 6)).getBrgys();
			city_mun_el.select2();
		},1000);		
		
		setTimeout(function(){
			brgy_district_el.select2('destroy').val(pad("0" + brgy_district, 9));
			brgy_district_el.select2();
		},2000);
	});
	function pad (str, max) {
	  str = str.toString();
	  return str.length < max ? pad("0" + str, max) : str;
	}
	/**-------------------------------- Address Ajax Loader --------------------------------------------------------------**/

});	
	 