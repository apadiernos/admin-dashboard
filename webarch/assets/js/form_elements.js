//simple file upload
$(document).on('change', '.bs-file', function(e) {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);

});

$(document).ready(function(){
	/**-------------------------------- Add/Remove Rows Methods --------------------------------------------------------------**/	
	 
	/**
	 *  @params targetELement object rows parent element
	 *  @description iterate all child rows and refactors their name
	 */	 
	function refactoringnameattr(targetELement){
		var targetELement = targetELement ? targetELement : '';
		$(targetELement+' .row-form-turnilyo').each(function(index){
			$(this).refactornameIndex(index);
		})
	}	
	$.fn.reinitializedjsplugin = function(){
		this.find(".source").select2('destroy');
		this.find(".select2-container.source").remove();
		this.find(".source").select2();
		
		this.find("textarea.text-editor").siblings().remove();
		this.find("textarea.text-editor").show();
		this.find("textarea.text-editor").wysihtml5();
		
		dropzone_init(this.find('.dropzone'));
	};
	
	/**
	 *  @params index int iterative number
	 *  @description refactor all names in an arraylist format based on the given index. obj[nestedobj][index][property]
	 */	 
	$.fn.refactornameIndex = function(index){
		this.find('input:not(input[type="submit"]),textarea,select').each(function(){
			var el = $(this);
			var obj = el.attr('obj');
			var nestedobj = el.attr('nestedObj');
			var propertyStr = el.attr('property');
			var property = propertyStr && propertyStr.indexOf("[") > -1 ? propertyStr : '['+propertyStr+']';

			if(  nestedobj && property ){
				el.attr('name',obj+'['+nestedobj+']['+index+']'+property).attr('id',obj+'['+nestedobj+']['+index+']'+property);
			}else if( property ){
				el.attr('name',obj+'['+index+']'+property).attr('id',obj+'['+index+']'+property);
			}else{
				el.attr('name',obj+'['+index+']').attr('id',obj+'['+index+']');
			}
		});	   
	};
	/**-------------------------------- Add/Remove Rows Methods --------------------------------------------------------------**/		
	
	/**-------------------------------- Jquery Plugins Initialization --------------------------------------------------------------**/
	//simple file upload
	var base_url = $('body').attr("base_url");
    $('body').on('fileselect','.bs-file', function(event, numFiles, label) {
		var tmppath = URL.createObjectURL(event.target.files[0]);
		var imgSrc = $(this).parents('.single-image-upload').find('.bs-file-img');
		var labelSrc = $(this).parents('.single-image-upload').find('.bs-file-input');
		if( imgSrc ) imgSrc.attr('src',tmppath);		
        if( labelSrc ) labelSrc.val(label);
    });

	//Dropdown menu - select2 plug-in
	 $(".source").select2();
	  
	//HTML5 editor
	$('.text-editor').each(function(){
		$(this).wysihtml5();
	});
	
	//Date Pickers
	$('.input-append.date').datepicker({
		autoclose: true,
		todayHighlight: true
	});
	   
	//Drag n Drop up-loader
	dropzone_init($(".dropzone"));
	/**-------------------------------- Jquery Plugins Initialization --------------------------------------------------------------**/
	
	/**-------------------------------- Add/Remove Rows --------------------------------------------------------------**/
	$('body').on('click',".add-row",function(e){
		e.preventDefault();
		var targetELement = $(this).attr('target');

		if( $(this).attr('data-cloned-template') ){
			var clonedElement = $(this).attr('data-cloned-template');
			var cloned_template = $(clonedElement).html();
		
		}else
			var cloned_template = $('#cloned-template').html();
		$(targetELement).append(cloned_template);
		$(targetELement+ ' .row-form-turnilyo:last').find('input:not(input[type="submit"],[defaultval="1"]),textarea,select').val('');

		refactoringnameattr(targetELement);
		$(targetELement+ ' .row-form-turnilyo:last').reinitializedjsplugin();
		
		return false;
	});
	$('body').on('click','.delete-row',function(e){
		e.preventDefault();
		var rowELement = $('.row-form-turnilyo');
		//for rows with attachments
		if ( $(this).attr('module') && $(this).attr('module_id') && $(this).attr('module_id') != 0 ){
			var module = $(this).attr('module');
			var id = $(this).attr('module_id');
						
			//for weblayout row removals
			$.ajax({
				type: 'POST',
				url: base_url+'admin/weblayouts/remove',
				data: {
					'layout' : module,
					'id' : id
				},
				success: function (attachments) {
					console.log(attachments);
				}
			});	
			
			$.ajax({
				type: 'POST',
				url: dropZOneremoveurl,
				data: {
					'module' : module,
					'module_id' : id
				},
				success: function (attachments) {
					//console.log(attachments);
				}
			});
		
		}
		
		var targetELement = $(this).attr('target');
		if( $(targetELement+' .row-form-turnilyo').length > 1 )
			$(this).parents('.row-form-turnilyo').remove();
		
		
		refactoringnameattr(targetELement);
		return false;
	});  
	/**-------------------------------- Add/Remove Rows --------------------------------------------------------------**/

	/**-------------------------------- DROPZONE JS INIT --------------------------------------------------------------**/
	/**
	 *  @params elDropzone object dropzone element
	 *  @description use to initialize dropzone file upload
	 */	 
	function dropzone_init(elDropzone){
		var maxFiles = elDropzone.attr('maxfiles');
		var dropZOneurl = base_url+'attachment/dropzoneUpload';
		var dropZOneremoveurl = base_url+'attachment/dropzoneRemoveFiles';

		elDropzone.dropzone({    
			maxFiles : maxFiles,
			url : dropZOneurl,
			addRemoveLinks : true,	
			removedfile: function(file) {     
				var moduleId = this.element.getAttribute('module_id');
				var module = this.element.getAttribute('module');	
				var fileId = $(file.previewElement).attr('fileId');	
				var moduleRef = $(file.previewElement).attr('module_ref');	
				$.ajax({
					type: 'POST',
					url: dropZOneremoveurl,
					data: {
						'module' : module,
						'module_id' : moduleId,
						'module_ref' : moduleRef,
						'id' : fileId,
					},
					success: function (attachments) {
						//console.log(attachments);
					}
				});				
				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
			},	
			init: function () {
				var moduleId = this.element.getAttribute('module_id');
				var module = this.element.getAttribute('module');		
				var moduleRef = this.element.getAttribute('module_ref');		
				var dropEl = this;
				
				$.ajax({
					type: 'POST',
					url: base_url+'attachment/dropzoneGetFiles',
					data: {
						'module' : module,
						'module_id' : moduleId,
						'module_ref' : moduleRef,
					},			
					success: function (attachments) {
						var jsonAttachments = $.parseJSON(attachments);
						$.each(jsonAttachments,function(key,attachment){
							if(key != 'dir'){
				
								var fileDir = create_upload_dir(attachment.date_created)+attachment.file_raw_name+'-100x100'+attachment.file_ext;
								var mockFile = { name: attachment.file_name, size: attachment.file_size, type: attachment.file_type };
								console.log(mockFile);
								
								dropEl.options.addedfile.call(dropEl, mockFile)
								$(mockFile.previewElement).attr('fileId', attachment.id);
								//dropEl.addFile.call(dropEl, mockFile);
								dropEl.options.thumbnail.call(dropEl, mockFile, fileDir);
								
							}
						});
					}
				});		
				
				this.on("sending", function(file, xhr, formData) {
					var moduleId = this.element.getAttribute('module_id');
					var module = this.element.getAttribute('module');
					var moduleRef = this.element.getAttribute('module_ref');				
					formData.append('module', module);
					formData.append('module_id', moduleId);
					formData.append('module_ref', moduleRef);
					
				});
				this.on("complete", function (data) {
					var dataTarget = this.element.getAttribute('data-target');
					if( data.status == "error" )
						$(data.previewTemplate).remove();
					else{
						var addedAttachments = $.parseJSON(data.xhr.responseText);
						console.log(dataTarget);
						$(data.previewElement).each(function(key){
							$(this).attr('fileId',addedAttachments[key].id);
							if(dataTarget && dataTarget.length > 0)
								$(dataTarget).val(addedAttachments[key].id);
						})
					}
				});
			}

		
		});		
	}
	/**-------------------------------- DROPZONE JS INIT --------------------------------------------------------------**/
});

