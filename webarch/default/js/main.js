$(document).ready(function(){
	var base_url = $('body').attr("base_url");
	
	/**-------------------------------- wysihtml5 JS --------------------------------------------------------------**/
	$('#text-editor').wysihtml5();
	/**-------------------------------- wysihtml5 JS --------------------------------------------------------------**/
	
	/**-------------------------------- DROPZONE JS --------------------------------------------------------------**/
	var maxFiles = $(".dropzone").attr('maxfiles');
	var dropZOneurl = base_url+'attachment/dropzoneUpload';
	var dropZOneremoveurl = base_url+'attachment/dropzoneRemoveFiles';

	$(".dropzone").dropzone({    
		maxFiles : maxFiles,
		url : dropZOneurl,
		addRemoveLinks : true,	
		removedfile: function(file) {     
			var moduleId = this.element.getAttribute('module_id');
			var module = this.element.getAttribute('module');	
			var fileId = $(file.previewElement).attr('fileId');	
			var moduleRef = $(file.previewElement).attr('module_ref');	
			$.ajax({
				type: 'POST',
				url: dropZOneremoveurl,
				data: {
					'module' : module,
					'module_id' : moduleId,
					'module_ref' : moduleRef,
					'id' : fileId,
				},
				success: function (attachments) {
					//console.log(attachments);
				}
			});				
			var _ref;
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
        },	
        init: function () {
			var moduleId = this.element.getAttribute('module_id');
			var module = this.element.getAttribute('module');		
			var moduleRef = this.element.getAttribute('module_ref');		
			var dropEl = this;
			
			$.ajax({
				type: 'POST',
				url: base_url+'attachment/dropzoneGetFiles',
				data: {
					'module' : module,
					'module_id' : moduleId,
					'module_ref' : moduleRef,
				},			
				success: function (attachments) {
					var jsonAttachments = $.parseJSON(attachments);
					$.each(jsonAttachments,function(key,attachment){
						if(key != 'dir'){
			
							var fileDir = create_upload_dir(attachment.date_created)+attachment.file_raw_name+'-100x100'+attachment.file_ext;
							var mockFile = { name: attachment.file_name, size: attachment.file_size, type: attachment.file_type };
							console.log(mockFile);
							
							dropEl.options.addedfile.call(dropEl, mockFile)
							$(mockFile.previewElement).attr('fileId', attachment.id);
							//dropEl.addFile.call(dropEl, mockFile);
							dropEl.options.thumbnail.call(dropEl, mockFile, fileDir);
							
						}
					});
				}
			});		
			
			this.on("sending", function(file, xhr, formData) {
				var moduleId = this.element.getAttribute('module_id');
				var module = this.element.getAttribute('module');
				var moduleRef = this.element.getAttribute('module_ref');				
				formData.append('module', module);
				formData.append('module_id', moduleId);
				formData.append('module_ref', moduleRef);
				
			});
            this.on("complete", function (data) {
				var dataTarget = this.element.getAttribute('data-target');
				if( data.status == "error" )
					$(data.previewTemplate).remove();
				else{
					var addedAttachments = $.parseJSON(data.xhr.responseText);
					console.log(dataTarget);
					$(data.previewElement).each(function(key){
						$(this).attr('fileId',addedAttachments[key].id);
						if(dataTarget && dataTarget.length > 0)
							$(dataTarget).val(addedAttachments[key].id);
					})
				}
            });
        }

	
    });
	/**-------------------------------- DROPZONE JS --------------------------------------------------------------**/
	
});