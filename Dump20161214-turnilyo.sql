-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: turnilyo
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attachments`
--

DROP TABLE IF EXISTS `attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(2000) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `file_size` decimal(18,2) DEFAULT NULL,
  `file_ext` varchar(255) DEFAULT NULL,
  `file_content_length` int(11) DEFAULT NULL,
  `file_raw_name` varchar(255) DEFAULT NULL,
  `alt` varchar(2000) DEFAULT NULL,
  `title` varchar(2000) DEFAULT NULL,
  `module` varchar(128) DEFAULT NULL,
  `module_id` int(11) DEFAULT '0',
  `module_ref` varchar(255) DEFAULT NULL,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachments`
--

LOCK TABLES `attachments` WRITE;
/*!40000 ALTER TABLE `attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `description` longtext,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parts_brands`
--

DROP TABLE IF EXISTS `parts_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parts_brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(255) DEFAULT NULL,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parts_brands`
--

LOCK TABLES `parts_brands` WRITE;
/*!40000 ALTER TABLE `parts_brands` DISABLE KEYS */;
INSERT INTO `parts_brands` VALUES (1,'brand1',1481008430,0,1,0),(2,'brand2',1481008430,0,1,0),(3,'brand3',1481008430,0,1,0),(4,'brand4',1481008776,0,1,0),(5,'brand5',1481008785,0,1,0),(6,'brand6',1481008833,0,1,0),(7,'brand7',1481008839,0,1,0),(8,'brand8',1481008871,0,1,0),(9,'brand8',1481009133,0,1,0),(10,'brand8',1481009251,0,1,0),(11,'brand8',1481009473,0,1,0),(12,'test',1481009479,0,1,0),(13,'test2',1481009506,0,1,0),(14,'brand1 eited',1481610978,0,1,0),(15,'brand2 new edit 1',1481610978,1481611139,1,1),(16,'brand3 new',1481611139,0,1,0);
/*!40000 ALTER TABLE `parts_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parts_categories`
--

DROP TABLE IF EXISTS `parts_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parts_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parts_categories`
--

LOCK TABLES `parts_categories` WRITE;
/*!40000 ALTER TABLE `parts_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `parts_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_has_contactinfos`
--

DROP TABLE IF EXISTS `post_has_contactinfos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_has_contactinfos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT '0',
  `tag` varchar(64) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_has_contactinfos`
--

LOCK TABLES `post_has_contactinfos` WRITE;
/*!40000 ALTER TABLE `post_has_contactinfos` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_has_contactinfos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_has_locations`
--

DROP TABLE IF EXISTS `post_has_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_has_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT '0',
  `lattitude` int(11) DEFAULT '0',
  `longitude` int(11) DEFAULT '0',
  `location` longtext,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_has_locations`
--

LOCK TABLES `post_has_locations` WRITE;
/*!40000 ALTER TABLE `post_has_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_has_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_has_owners`
--

DROP TABLE IF EXISTS `post_has_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_has_owners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT '0',
  `firstname` varchar(255) DEFAULT '0',
  `lastname` varchar(255) DEFAULT '0',
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_has_owners`
--

LOCK TABLES `post_has_owners` WRITE;
/*!40000 ALTER TABLE `post_has_owners` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_has_owners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_has_parts`
--

DROP TABLE IF EXISTS `post_has_parts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_has_parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT '0',
  `parts_category_id` int(11) DEFAULT '0',
  `parts_brand_id` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT '0',
  `price` float DEFAULT '0',
  `description` longtext,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_has_parts`
--

LOCK TABLES `post_has_parts` WRITE;
/*!40000 ALTER TABLE `post_has_parts` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_has_parts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_has_services`
--

DROP TABLE IF EXISTS `post_has_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_has_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT '0',
  `service_type_id` int(11) DEFAULT '0',
  `price` float DEFAULT '0',
  `labor` tinyint(1) DEFAULT '0',
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `service` varchar(255) DEFAULT NULL,
  `description` longtext,
  `is_service_type` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_has_services`
--

LOCK TABLES `post_has_services` WRITE;
/*!40000 ALTER TABLE `post_has_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_has_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_has_staff`
--

DROP TABLE IF EXISTS `post_has_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_has_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT '0',
  `staff_skill_id` int(11) DEFAULT '0',
  `firstname` varchar(255) DEFAULT '0',
  `lastname` varchar(255) DEFAULT '0',
  `description` longtext,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_has_staff`
--

LOCK TABLES `post_has_staff` WRITE;
/*!40000 ALTER TABLE `post_has_staff` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_has_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_types`
--

DROP TABLE IF EXISTS `service_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_types`
--

LOCK TABLES `service_types` WRITE;
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(255) DEFAULT NULL,
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_has_skills`
--

DROP TABLE IF EXISTS `staff_has_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_has_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT '0',
  `skill_id` int(11) DEFAULT '0',
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_has_skills`
--

LOCK TABLES `staff_has_skills` WRITE;
/*!40000 ALTER TABLE `staff_has_skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_has_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` int(10) NOT NULL DEFAULT '0',
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email_address` varchar(500) DEFAULT NULL,
  `mobile_number` varchar(30) DEFAULT NULL,
  `first_login` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `timecreated` int(11) DEFAULT '0',
  `timeupdated` int(11) DEFAULT '0',
  `createdby` int(11) DEFAULT '0',
  `updatedby` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin',2,'Turnilyo','Admin','admin@turnilyo.com','123456',0,1,0,0,1,1),(2,'cadmin','cadmin',1,'Turnilyo','CAdmin','cadmin@turnilyo.com','123456',0,1,0,0,1,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersession`
--

DROP TABLE IF EXISTS `usersession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersession` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(32) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text,
  `timestamp` int(10) NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersession`
--

LOCK TABLES `usersession` WRITE;
/*!40000 ALTER TABLE `usersession` DISABLE KEYS */;
INSERT INTO `usersession` VALUES ('2j47lrndutl8iu1js8ck3p170kud2cuk','127.0.0.1',NULL,0,NULL,1481705094,'__ci_last_regenerate|i:1481679888;user_name|s:14:\"Turnilyo Admin\";user_id|s:1:\"1\";user_email|s:18:\"admin@turnilyo.com\";user_role|s:1:\"2\";'),('ua6pc6q6g42dsn6ufk8jbc9dlni9p105','127.0.0.1',NULL,0,NULL,1481679888,'__ci_last_regenerate|i:1481679888;');
/*!40000 ALTER TABLE `usersession` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-14 16:46:00
