<?php
$lang['site_title'] = 'Turnilyo Builder';
$lang['user_role_error'] = 'You do not have permission to access this site!';
$lang['login_error'] = 'Invalid username or Password';
$lang['message_successfully_logged_off'] = 'Logged Out Successfully!';

$lang['submit_success'] = 'Form Successfully Submitted!';

//sidebar navigation
$lang['nav_dashboard'] = 'Dashboard';
$lang['nav_email'] = 'Email';
$lang['nav_posts'] = 'Posts';
$lang['nav_records'] = 'Records';
$lang['nav_parts'] = 'Parts';
$lang['nav_parts_brands'] = 'Brands';
$lang['nav_parts_categories'] = 'Categories';
$lang['nav_skills'] = 'Skills';
$lang['nav_services'] = 'Services';
$lang['nav_settings'] = 'Settings';
$lang['nav_users'] = 'Users';
$lang['nav_add_new'] = 'Add New';
$lang['nav_listall'] = 'List';

//title pages
$lang['title_dashboard'] = 'Dashboard';
$lang['title_users_add'] = 'Add New - <span class="semi-bold">User</span>';
$lang['title_users_edit'] = 'Edit - <span class="semi-bold">User</span>';
$lang['title_users_list'] = 'Users <span class="semi-bold">List</span>';

$lang['title_post_add'] = 'Add New - <span class="semi-bold">Post</span>';
$lang['title_post_edit'] = 'Edit - <span class="semi-bold">Post</span>';
$lang['title_post_list'] = 'Post <span class="semi-bold">List</span>';

$lang['title_post_categories_add'] = 'Add New - <span class="semi-bold">Post Category</span>';
$lang['title_post_categories_edit'] = 'Edit - <span class="semi-bold">Post Category</span>';
$lang['title_post_categories_list'] = 'Post <span class="semi-bold">Categories</span>';

$lang['title_brands_add'] = 'Add New - <span class="semi-bold">Brand</span>';
$lang['title_brands_edit'] = 'Edit - <span class="semi-bold">Brand</span>';
$lang['title_brands_list'] = 'Brands <span class="semi-bold">List</span>';

$lang['title_categories_add'] = 'Add New - <span class="semi-bold">Category</span>';
$lang['title_categories_edit'] = 'Edit - <span class="semi-bold">Category</span>';
$lang['title_categories_list'] = 'Category <span class="semi-bold">List</span>';

$lang['title_skills_add'] = 'Add New - <span class="semi-bold">Skills</span>';
$lang['title_skills_edit'] = 'Edit - <span class="semi-bold">Skill</span>';
$lang['title_skills_list'] = 'Skill <span class="semi-bold">List</span>';

$lang['title_services_add'] = 'Add New - <span class="semi-bold">Services</span>';
$lang['title_services_edit'] = 'Edit - <span class="semi-bold">Service</span>';
$lang['title_services_list'] = 'Services <span class="semi-bold">List</span>';


//--Parts Brands
//form & list
$lang['brand'] = 'Brand';

//--Parts Cateories
//form & list
$lang['category'] = 'Category';

//--Skills
//form & list
$lang['skill'] = 'Skill';

//--Services
//form & list
$lang['service'] = 'Service';

//--Users
//form & list
$lang['user'] = 'User';
$lang['first_name'] = 'First Name';
$lang['last_name'] = 'Last Name';
$lang['role'] = 'Role';
$lang['birthdate'] = 'Date of Birth';
$lang['male'] = 'Male';
$lang['female'] = 'Female';
$lang['email_address'] = 'Email Address';
$lang['address_line1'] = 'Address Line 1';
$lang['address_line2'] = 'Address Line 2';
$lang['brgy_district'] = 'Brgy/District';
$lang['city_municipality'] = 'City/Municipality';
$lang['province'] = 'Province';
$lang['postal_code'] = 'Postal Code';
$lang['mobile_number'] = '11 digits mobile number';
$lang['role_super_admin'] = 'Super Admin';
$lang['role_client_admin'] = 'Client Admin';
$lang['basic_information'] = 'Basic Information';
$lang['postal_information'] = 'Postal Information';
$lang['profile_picture'] = 'Profile Picture';
$lang['password'] = 'Password';
$lang['username'] = 'Username';
$lang['sadmin'] = 'Sadmin';
$lang['cadmin'] = 'Cadmin';
$lang['username_taken'] = 'Username Already Taken!';
$lang['email_taken'] = 'Email Already Taken!';

//--Post / Ads
//form & list
$lang['client'] = 'Client';
$lang['description'] = 'Description';
$lang['other_nformation'] = 'Other Information';
$lang['company_logo'] = 'Company Logo';
$lang['locations'] = 'Locations';
$lang['contact_information'] = 'Contact Information';
$lang['owners'] = 'Owners';
$lang['parts_accessories'] = 'Parts and Accessories';
$lang['services'] = 'Services';
$lang['staff'] = 'Staff';
$lang['post_exists'] = 'Post Exists';
$lang['shop'] = 'Shop';
$lang['contact'] = 'Contact';
$lang['information'] = 'Information';
$lang['parts'] = 'Parts';
$lang['accessories'] = 'Accessories';
$lang['name'] = 'Name';
$lang['price'] = 'Price';
$lang['display_price'] = 'Display Price?';
$lang['photos'] = 'Photos';
$lang['including_labor'] = 'Including Labor?';
$lang['service_type_as_name'] = 'Service Type as Name?';

//list
$lang['createdby'] = 'Created By';
$lang['datecreated'] = 'Date Created';

//buttons
$lang['remove'] = 'Remove';
$lang['save'] = 'Save';
$lang['cancel'] = 'Cancel';
$lang['list'] = 'List';
$lang['edit'] = 'Edit';

//misc
$lang['addnew'] = 'Add New';
$lang['choose'] = 'Please Select';