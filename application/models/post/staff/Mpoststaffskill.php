<?php
class Mpoststaffskill extends Secure_Model
{
	function __construct()
    {
		parent::__construct();
    }
	
	/**
	* @description get records form poststaffskills table using parameters
	* @params array $wheres where parameters to filter query
	* @params array $paging offset and limit
	* @params array $orderby sort
	* @return stdClass Object arrayList of poststaffskills results
	*/		
    function getAll($wheres=null,$paging=null,$orderby=null)
    {
    	$result = array();
		
		
		$this->db->select("		
			poststaffskill.*,
			concat_ws(' ', userUp.first_name, userUp.last_name) As nameEditor,
		")->from($this->dbstaffhasskills.' poststaffskill');
			
		//get query params
		$this->getQuery('poststaffskill',$wheres,$paging,$orderby);
		
		$query = $this->db->get();

		return $query->result();
    }
 
 	/**
	* @description get record form poststaffskills table using parameters
	* @params array $wheres where parameters to filter query
	* @return stdClass Object of poststaffskills single result
	*/
    function get($wheres)
    {
		
		$this->db->select("
			poststaffskill.*,
			concat_ws(' ', userUp.first_name, userUp.last_name) As nameEditor,
			")->from($this->dbstaffhasskills.' poststaffskill');

		//get query params
		$this->getQuery('poststaffskill',$wheres,$paging,$orderby);
			
		$query = $this->db->get()->row();
		return $query;
    }
    
 	/**
	* @description insert or update record
	* @params array $data record data to be inserted or updated
	* @return integer id of inserted or updated record
	*/	
    function saveorupdate($data)
    {
		if( !isset($data['id']) || $data['id'] == 0 ){
			$data['createdby'] = $this->user_id;
			$data['timecreated'] = time();
			$this->db->insert($this->dbstaffhasskills, $data);
			$id = $this->db->insert_id();
		}else{
			$data['updatedby'] = $this->user_id;
			$data['timeupdated'] = time();
			$this->db->where('id',  $data['id']);
			$id = $data['id'];
			unset($data['id']);
			$this->db->update($this->dbstaffhasskills, $data);
		}
		return $id;
    }
    
 	/**
	* @description deletes record
	* @params array $data filter parameters data to find a record
	*/		
	function delete($wheres)
    {
		foreach($wheres as $column => $value)
			$this->db->where($column, $value);

		$this->db->delete($this->dbstaffhasskills);
    }
	
 	/**
	* @description count records of data
	* @params array $wheres filter parameters data to find records
	* @return stdClass Object data of the returned record
	*/		
    function countAll($wheres=null)
    {
		$this->db->select('COUNT(id) as total')->from($this->dbstaffhasskills);
		
		if( $wheres ){
			foreach($wheres as $column => $value)
				$this->db->where($column, $value);
		}
		$query = $this->db->get()->row();
		return $query;
    }
}

