<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attachmentlib  {
	
	private $uploadDir;
	private $formData;
	
	
	protected $CI;
	
	public function __construct(array $config = array())
	{
       
	   $this->CI =& get_instance();
	   $this->CI->load->helper(array('url','html','form'));
	   $this->CI->load->model( array('mattachment') );
	   $this->CI->config->load('images');
	   
	   $this->uploadDir = FCPATH.'uploads/';
	   $this->formData = array();
	   $this->formData = $config;

	   log_message('info', 'Atachment Lib Class Initialized');
	}
	
	public function getuploadsDir(){
		$dir = $this->uploadDir.'/'.date("Y").'/'.date('m').'/';
		if (!file_exists($dir)) {
			mkdir($dir,0777, true);
		}			
		return $dir;
		
	}
	
    public function upload_files() {
		global $_FILES;
		$upload_path = $this->getuploadsDir();
		$this->CI->load->library(array('upload'));
		$this->CI->load->helper('file');
		
		$config['upload_path'] = $this->getuploadsDir();
		$config['allowed_types'] = '*';
		$config['max_size']    = '999999999';
		$config['remove_spaces']  = TRUE;
		$config['overwrite'] = TRUE;

		
		$sizes = $this->CI->config->item("image_sizes");
		$dataFiles = array();
		$formData = $this->CI->input->post();
		$formData = $formData['module_id'] ? $this->CI->input->post() : $this->formData;

		foreach ($_FILES as $key => $value) {
			if( file_exists($upload_path .'/'.$value['name']) ){
				$new_file_name = explode(".", $value['name']);
				$config['file_name'] = $new_file_name[0].'-1';
			}
			$this->CI->upload->initialize($config);
			$this->CI->upload->do_upload($key);
			$fileupload = $this->CI->upload->data();
			
			if( $fileupload['is_image'] == 1 ){		
				foreach( $sizes as $size ){
					$this->crop($fileupload,$size[0],$size[1]);
				}		
			}
			
			$dataFile['file_name'] = str_replace(",","-",$fileupload['file_name']);
			$dataFile['file_raw_name'] = $fileupload['raw_name'];
			$dataFile['file_type'] = $fileupload['file_type'];
			$dataFile['file_size'] = $fileupload['file_size'];
			$dataFile['file_ext'] = $fileupload['file_ext'];	
			$dataFile['module'] = $formData['module'];	
			$dataFile['module_id'] = $formData['module_id'];	
			$dataFile['module_ref'] = $formData['module_ref'];	
			$dataFile['orphan'] = $formData['orphan'] ? $formData['orphan'] : 1;	
			$id = $this->CI->mattachment->saveorupdate($dataFile);
			$dataFile['id'] = $formData['id'] ? $formData['id'] : $id;
			
			$dataFiles[] = $dataFile;
		}
		
		if( isset($dataFiles) && count($dataFiles) > 1 )
			echo json_encode($dataFiles);
    }
	
	public function get_files($return=null) {
		
		$formData = $this->CI->input->post();
		$formData = $formData['module_id'] ? $this->CI->input->post() : $this->formData;

		$dataFile['module'] = $formData['module'];	
		$dataFile['module_id'] = $formData['module_id'];			
		$dataFile['module_ref'] = $formData['module_ref'];			
		$attachments = $this->CI->mattachment->getAll($dataFile);
		if($attachments) $attachments['dir'] = base_url().'/uploads/';

		if( $return )
			return $attachments;
		else
			echo json_encode($attachments);
	}
	
	public function remove_files($id=null) {
		
		$formData = $this->CI->input->post();
		$formData = $formData['module_id'] ? $this->CI->input->post() : $this->formData;
		
		$dataFile['module'] = $formData['module'];	
		$dataFile['module_id'] = $formData['module_id'];
		$dataFile['id'] = $id ? $id : $formData['id'];
		$attachments = $this->CI->mattachment->getAll($dataFile);
		$this->CI->mattachment->delete($dataFile);
		
		$sizes = $this->CI->config->item("image_sizes");
		foreach( $attachments as $attachment){	
			$dateCreated = $attachment->timecreated;
			$upload_path = $this->uploadDir .'/'.date("Y",$dateCreated).'/'.date('m',$dateCreated).'/';
			$file = $upload_path .'/'.$attachment->file_name;
				unlink($file);
			foreach( $sizes as $size ){
				$file = $upload_path .'/'.$attachment->file_raw_name.'-'.$size[0].'x'.$size[1].$attachment->file_ext;
				if( file_exists($file) )
					unlink($file);
			}
		}
	}
	
	
	public function crop($image_data, $width, $height)
    {
		$upload_path = $this->getuploadsDir();
		$w_orig = $image_data['image_width'];
		$h_orig = $image_data['image_height'];
		
		$w_thumb = $width;
		$h_thumb = ( $h_orig * $width ) / $w_orig;
		
		$y_thumb = 0;
		$x_thumb = 0;
		
		if ($h_thumb > $height) {
			$y_thumb = ($h_thumb - $height) / 3;
		} else {
			$h_thumb = $height;
			$w_thumb = ($w_orig * $height) / $h_orig;
			$x_thumb = ($w_thumb - $width) / 3;
		}
		
		$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $upload_path .'/'.$image_data['raw_name'].'-'.$width.'x'.$height.$image_data['file_ext'],
			'maintain_ratio' => TRUE,
			'width' => $width,
			'height' => $height,								
			'image_library' => 'gd2',				
			'dynamic_output' => FALSE,				
		);	
		$this->CI->load->library('image_lib', $config);
		$this->CI->image_lib->clear();
		$this->CI->image_lib->initialize($config);			
		$this->CI->image_lib->fit();

    }	
}
 