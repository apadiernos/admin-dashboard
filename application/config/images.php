<?php
/*
|--------------------------------------------------------------------------
| Image Preset Sizes
|--------------------------------------------------------------------------
|
| Specify the preset sizes you want to use in your code. Only these preset 
| will be accepted by the controller for security.
|
| Each preset exists of a width and height. If one of the dimensions are 
| equal to 0, it will automatically calculate a matching width or height 
| to maintain the original ratio.
|
| If both dimensions are specified it will automatically crop the 
| resulting image so that it fits those dimensions.
|
*/

$config["image_sizes"]["square1"] = array(60, 60);
$config["image_sizes"]["square2"] = array(100, 100);
$config["image_sizes"]["square3"] = array(130, 130);
$config["image_sizes"]["square4"] = array(300, 300);

$config["image_sizes"]["long1"]   = array(230, 300);

$config["image_sizes"]["wide1"]   = array(224, 132);
$config["image_sizes"]["wide2"]   = array(230, 160);
$config["image_sizes"]["wide3"]   = array(285, 193);
$config["image_sizes"]["wide4"]   = array(306, 180);
$config["image_sizes"]["wide5"]   = array(306, 189);

$config["image_sizes"]["large1"]   = array(638, 280);
$config["image_sizes"]["large2"]   = array(700, 200);
$config["image_sizes"]["large3"]   = array(700, 420);

