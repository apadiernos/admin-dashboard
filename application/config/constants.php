<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*
|--------------------------------------------------------------------------
| Docment root folders
|--------------------------------------------------------------------------
|
| These constants use existing location information to work out web root, etc.
|
*/

// Base URL (keeps this crazy sh*t out of the config.php
if (isset($_SERVER['HTTP_HOST']))
{
    $base_url  = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on' ? 'https' : 'http';
    $base_url .= '://'. $_SERVER['HTTP_HOST'];
    $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

    // Base URI (It's different to base URL!)
    $base_uri = parse_url($base_url, PHP_URL_PATH);

    if (substr($base_uri, 0, 1) != '/')
    {
        $base_uri = '/'.$base_uri;
    }

    if (substr($base_uri, -1, 1) != '/')
    {
        $base_uri .= '/';
    }
}

else
{
    $base_url = 'http://montpac-portal.com/';
    $base_uri = '/eformsv2/';
}

// Define these values to be used later on
define('BASE_URL', $base_url);
define('BASE_URI', $base_uri);
define('APPPATH_URI', BASE_URI.APPPATH);

// We dont need these variables any more
unset($base_uri, $base_url); 


/*
|--------------------------------------------------------------------------
| Status Contants
|--------------------------------------------------------------------------
|
| These constants use in status tables.
|
*/

//User Roles
define('SUPERADMIN', 2);
define('ADMIN', 1);

define('ACTIVE', 1);
define('INACTIVE', 0);


//For Messages
define('IS_ERROR', -1);
define('IS_SUCCESS', 0);

//role constants globals
$GLOBALS['role_constants'] = array(
	SUPERADMIN => array(
					'view'=>'sadmin',
					'dashboard'=>'sdashboard',
				  ),
	ADMIN => array(
					'view'=>'admin',
					'dashboard'=>'dashboard',
				  ),
);
//default pages globals
$GLOBALS['default_pages'] = array(
			"add" => "save",
			"edit" => "save",
			"list" => "list",
		);
define('ADMIN_EMAIL', 'admin@montpac.com');
date_default_timezone_set('Asia/Manila');
/* End of file constants.php */
/* Location: ./application/config/constants.php */