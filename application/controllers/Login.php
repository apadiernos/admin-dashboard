<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Login Class
 *
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Login extends CI_Controller
{
	function __construct()
    {
    	parent::__construct();
    	$this->load->model( array('sadmin/muser') );
    }
    
	/**
	* @description loads login form
	*/		
    public function index()
    {
    	global $role_constants;
		$this->load->view('login/header');
		if( $this->session->userdata('user_id') && $this->session->userdata('user_role') ){
			$dashhome = $role_constants[$this->session->userdata('user_role')]['dashboard'] ? redirect($role_constants[$this->session->userdata('user_role')]['dashboard']) : redirect('logout');
		}
    	$this->load->view('login/templates/v1');
    	$this->load->view('login/footer');
    }

	/**
	* @description login form submission process
	*/	
	function do_login()
    {
    	$txt_username = $this->input->post('txtusername');
        $txt_password = $this->input->post('txtpassword');

        if ($txt_username == '' OR $txt_password == '') 
        {
            set_messages(IS_ERROR, lang('login_error'));
            redirect('login');
        }
        else 
        {
	    	$paramsLogin = array(
				'username' => $txt_username,
				'password' => $txt_password,
				'status' => ACTIVE,
			);
			$get_user = $this->muser->get($paramsLogin);
	        if (count($get_user) > 0) 
	        {
				
	        	$user_role = $get_user->role;
	        	$user_id = $get_user->id;
	        	$user_email = $get_user->email_address;
	        	$user_role = $get_user->role;
	        	$user_name = $get_user->first_name." ".$get_user->last_name;

	        	$this->session->set_userdata('user_name', $user_name);
	        	$this->session->set_userdata('user_id', $user_id);
	        	$this->session->set_userdata('user_email', $user_email);
	        	$this->session->set_userdata('user_role', $user_role);

            	clear_messages();
	        	redirect('dashboard');
			}
	        else
	        {
	           set_messages(IS_ERROR, lang('login_error'));
	           redirect('login');
	        }        	
        }
    }
    
	/**
	* @description logout process and destroys all sessions
	*/		
	function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('message', lang('message_successfully_logged_off'));
        redirect('login');
    }
    
}