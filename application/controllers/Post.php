
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sadmin Class
 *
 * @description post Controllers and renderers
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class post extends Secure_Controller
{
	
	var $post_id = 0;
	var $user_id = 0;
	var $pages = array();
	var $provinces = "";
	var $parts;
	var $servicetypes;
	var $staffskills;
	var $formsession;
	var $postdata;
	
	function __construct()
    {
    	parent::__construct();
		$this->load->model( 
			array(
				'post/mpost',
				'post/mpostservice',
				'post/mpostpart',
				'post/mpostowner',
				'post/staff/mpoststaffskill',
				'post/mpoststaff',
				'post/mpostlocation',
				'post/mpostcontactinfo',
				'sadmin/parts/mpartsbrands',
				'sadmin/parts/mpartscategories',
				'sadmin/mservicetypes',
				'sadmin/mskills',
				'sadmin/muser',
				)
			);
		$this->user_id = $this->session->userdata('user_id');
		$this->load->helper(array('url','html','form','string'));
		
		$this->config->load('images');
		$this->ppsize = $this->config->item("image_sizes")['square1'];
		
		//get provinces
		$this->provinces = new stdClass;
		$this->provinces = getJson("address/refprovince");
		
		//get parts brands and categories
		$this->parts = new stdClass;
		$this->parts->brands = $this->mpartsbrands->getAll();
		$this->parts->categories = $this->mpartscategories->getAll();
		
		//get servicetypes
		$this->servicetypes = $this->mservicetypes->getAll();
		
		//get staffskils
		$this->staffskills = $this->mskills->getAll();
		
		//form session
		$this->formsession = random_string('unique');
		
		$this->session->userdata('user_role');
		$this->validate_permission(SUPERADMIN);
		
    }
	
	/**
	* @description renders post list
	*/		
    public function index()
    {
		
		$params = get_list_scripts();

		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('post') => base_url('post'),
			
		);
		//page title		
		$params['title'] = lang('title_post_list');
			
		$this->render('post/list',$params);			
    }
	
	/**
	* @description get default form parameters
	*/		
	private function post_form_params()
	{
		//provinces
		$params['provinces'] = $this->provinces;
		
		//parts brands and categories
		$params['partsdata'] = $this->parts;
		
		//servicetypes
		$params['servicetypes'] = $this->servicetypes;
		
		//staffskills
		$params['staffskills'] = $this->staffskills;
		
		//formsession
		$params['formsession'] = $this->formsession;

		return $params;
	}
	
	/**
	* @description renders post add
	*/		
    public function add()
    {
		$params = get_form_scripts();
		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('post') => base_url('post'),
			lang('addnew') => base_url('post/add'),
			
		);
			
		$params['title'] = lang('title_post_add');
		
		$params += $this->post_form_params();
		
		$this->render('post/save',$params);
    }
	
	/**
	* @renders post edit
	*/		
    public function edit()
    {
		$params = get_form_scripts();

		//get the post by id
		$postid = $this->uri->segment(3);
		if( $postid ){
			$params['post'] = $this->mpost->get(array('id'=>$postid));	
			//get profile picture file directory
			$paramsPP = array(
				'file_raw_name' => $params['post']->ppfile_raw_name,
				'file_ext' => $params['post']->ppfile_ext,
				'file_timecreated' => $params['post']->pptimecreated,
				'size' => 'square1',
			);
			$params['post']->profilepicture = getFileDirectory($paramsPP);
		}
		
		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('post') => base_url('post'),
			lang('addnew') => base_url('post/add'),
			
		);
		
		
		$params['title'] = lang('title_post_edit');

		$params += $this->post_form_params();
		
		$this->post_id = $postid;
		
		//gets location
		$params['locations'] = $this->get_location();
		//gets contactinfo
		$params['contactinfos'] = $this->get_contactinfo();
		//gets owners
		$params['owners'] = $this->get_owners();
		//gets parts
		$params['parts'] = $this->get_parts();
		//gets services
		$params['services'] = $this->get_services();
		//gets staff
		$params['staff'] = $this->get_staff();		
		
		$this->render('post/save',$params);
    }

	/**
	* @description remove post by id
	*/		
    public function remove()
    {
		$id = $this->uri->segment(4);
		$this->mpost->delete(array('id'=>$id));
    }
	
	/**----------------------------------------------------------------------------- GET ---------------------------------------------------------**/
	/**
	* @description get post locations
	* @return Object post locations
	*/		
	public function get_location(){
		$params['post_id'] = $this->post_id;
		return $this->mpostlocation->getAll($params);
	}
	/**
	* @description get post contactinfos
	* @return Object post contactinfos
	*/	
	public function get_contactinfo(){
		$params['post_id'] = $this->post_id;
		return $this->mpostcontactinfo->getAll($params);
	}	
	/**
	* @description get post owners
	* @return Object post owners
	*/		
	public function get_owners(){
		$params['post_id'] = $this->post_id;
		return $this->mpostowner->getAll($params);
	}
	/**
	* @description get post parts
	* @return Object post parts
	*/		
	public function get_parts(){
		$params['post_id'] = $this->post_id;
		return $this->mpostpart->getAll($params);		
	}
	/**
	* @description get post services
	* @return Object post services
	*/		
	public function get_services(){
		$params['post_id'] = $this->post_id;
		return $this->mpostowner->getAll($params);
	
	}
	/**
	* @description get post staff
	* @return Object post staff
	*/		
	public function get_staff(){
		$params['post_id'] = $this->post_id;
		return $this->mpoststaff->getAll($params);	
	}
	/**
	* @description get post via ajax
	*/			
	public function get_post(){
		$formData = $this->input->post();
		
		//sorting columns
		$orderby = null;
		if( $formData['order'] ){
			$columns = array('post.client','name','post.timecreated');
			$orderby["column"] = $columns[$formData['order'][0]['column']];
			$orderby["sort"] = $formData['order'][0]['dir'];
		}
		
		//pagination params
		$paging['limit'] = $formData['length'] ? $formData['length'] : 10;
		$paging['offset'] = $formData['start'] ? $formData['start'] : 0;
		
		//get records
		$post = $this->get_post_list($formData,$paging,$orderby);
				
		//total records
		if( $formData['search']['value']  ){
			$total = new stdClass;
			$total->total = count($post);
		}else
			$total = $this->mpost->countAll();
		
		//dtTable params
		$dtData = new stdClass;
		$dtData->offset = $formData['draw'] ? $formData['draw'] : 0;
		$dtData->recordsTotal = $total->total;
		$dtData->recordsFiltered = $total->total;

		$data = array();
		foreach($post as $key => $post){
			$dataInfo = array();
			if($post->nameEditor){
			  $dataInfo['Date Last Updated'] = format_time($post->timeupdated);
			  $dataInfo['Date Last Updated'] = format_time($post->timeupdated);
			  $dataInfo['Last Updated By'] = $post->nameEditor; 
			}

			$edit = '<a href="'.base_url('post/edit/'.$post->id).'" class="btn btn-success btn-cons"><i class="fa fa-pencil-square-o"></i>&nbsp;'.lang('edit').'</a>';
			$remove = '<a href="'.base_url('post/remove/'.$post->id).'" class="btn btn-danger btn-cons dt-row-remove"><i class="fa fa-times"></i>&nbsp;'.lang('remove').'</a>';
			$dataInfo[$edit] = $remove;			
			$data[$key] = array($post->client,$post->name,format_time($post->timecreated),json_encode($dataInfo));
		}	
		$dtData->dataList = $data;
		
		//dtTable accepted params
		$dtPrepared = $this->prepare_datatable_results($dtData);
		echo $dtPrepared;	
	}
	
	/**
	* @params array $formData form submission parameters to filter data
	* @params array $paging limit and offset for pagination
	* @description get list of records based on parameters
	*/		
	public function get_post_list($formData=null,$paging=null,$orderby=null){
		$params = null;
		if( isset($formData['search']) ){
			//if( $formData['search']['from'] != "" )	 $params['date_'.$this->statuspage[$status].' >='] = date("Y-m-d",strtotime($formData['search']['from']));
			//if( $formData['search']['to'] != "" )	 $params['date_'.$this->statuspage[$status].' <='] = date("Y-m-d",strtotime($formData['search']['to']));			
			if( $formData['search']['value'] != "" ){
				$searchTerm  = array(
					'value'=>$formData['search']['value'],
					'operator_method'=>'or_like'
				);	
				$params['client'] = $params['description'] = $params['user.first_name'] = $params['user.last_name'] = $params['user.email_address'] = $searchTerm;
				/**
				$params['amountwords'] = array(
					'value'=>$formData['search']['any'],
					'operator_method'=>'or_like',
					'group_start'=>true
				);
				$params['purpose'] = $params['remarks'] = $searchTerm;
				$params['amount '] = array(
					'value'=>$formData['search']['any'],
					'operator_method'=>'or_like',
					'group_end'=>true
				);
				**/
			}

		}
		//get brands
		return $this->mpost->getAll($params,$paging,$orderby);
	}	
	/**----------------------------------------------------------------------------- GET ---------------------------------------------------------**/

	/**----------------------------------------------------------------------------- SAVE ---------------------------------------------------------**/
	/**
	* @description save post records
	*/		
    public function save()
    {
		global $_FILES;
		$formData = $this->input->post();
		
		//saves post form data
		if( $formData ){
			
			$postdata = array(
				'client' => $formData['client'],
				'description' => $formData['description'],
			);
			$postid = $this->mpost->saveorupdate($postdata);
			set_messages(IS_SUCCESS, lang('submit_success'));
			if($postid){
				
				//saves logo
				if( $_FILES['single_image']['error'] == 0 ){
					//prepare post logo upload params
					$params = array('module_id' => $postid, 'module' => 'posts', 'module_ref'=>'logo', 'orphan'=>'0');
					$this->load->library('attachmentlib',$params);
					
					//check if post logo exists
					if( $plogo = $this->attachmentlib->get_files(1) ){
						$this->attachmentlib->remove_files($plogo[0]->id);
					}
					//upload post logo
					$this->attachmentlib->upload_files();	
				}
			}
			
			$this->postdata = $formData;
			$this->post_id = $postid;
			
			//saves location
			$this->save_location();
			//saves contactinfo
			$this->save_contactinfo();
			//saves owners
			$this->save_owners();
			//saves parts
			$this->save_parts();
			//saves services
			$this->save_services();
			//saves staff
			$this->save_staff();
			
			redirect('post/edit/'.$postid);
		}
	
		
    }
	/**
	* @description saves post locations
	*/		
	public function save_location(){
		if($this->postdata['location']){
			foreach( $this->postdata['location'] as $location ){
				if( !$location['address_line1'] ) continue;
				$location['post_id'] = $this->post_id;
				$this->mpostlocation->saveorupdate($location);
			}
		} 		
		
	}
	/**
	* @description saves post contactinfos
	*/	
	public function save_contactinfo(){
		if($this->postdata['contactinfo']){
			foreach( $this->postdata['contactinfo'] as $contactinfo ){
				if( !$contactinfo['value'] && !$contactinfo['tag'] ) continue;
				$contactinfo['post_id'] = $this->post_id;
				$this->mpostcontactinfo->saveorupdate($contactinfo);
			}
		} 		
		
	}	
	/**
	* @description saves post owners
	*/		
	public function save_owners(){
		if($this->postdata['owners']){
			foreach( $this->postdata['owners'] as $owner ){
				if( !$owner['first_name'] && !$owner['last_name'] && !$owner['username'] && !$owner['email_address'] ) continue;
				$userid = $this->save_user($owner);
				if($userid){
					//saves ownerdata
					$ownerdata = array(
						'post_id' => $this->post_id,
						'user_id' => $userid,
					);
					$this->mpostowner->saveorupdate($ownerdata);
				}
			}
		} 		
		
	}
	/**
	* @description saves post users
	* @param array $user user info
	* @return int $userid newly added or updated userid
	*/		
	public function save_user($user){
		//saves userdata
		$userdata = array(
			'first_name' => $user['first_name'],
			'last_name' => $user['last_name'],
			'username' => $user['username'],
			'email_address' => $user['email_address'],
		);
		$userid = $this->muser->saveorupdate($userdata);
		if($userid) return $userid;
	}
	/**
	* @description saves post parts
	*/		
	public function save_parts(){
		if($this->postdata['parts']){
			foreach( $this->postdata['parts'] as $part ){
				if( !$part['name'] ) continue;
				$part['post_id'] = $this->post_id;
				$this->mpostpart->saveorupdate($part);
			}
		} 		
	}
	/**
	* @description saves post services
	*/		
	public function save_services(){
		if($this->postdata['services']){
			foreach( $this->postdata['services'] as $service ){
				if( !$service['name'] ) continue;
				$service['post_id'] = $this->post_id;
				$this->mpostowner->saveorupdate($service);
			}
		} 		
	}
	/**
	* @description saves post staff
	*/		
	public function save_staff(){
		if($this->postdata['staff']){
			foreach( $this->postdata['staff'] as $staff ){
				if( !$staff['first_name'] && !$staff['last_name'] && !$staff['username'] && !$staff['email_address'] ) continue;
				$userid = $this->save_user($staff);
				if($userid){
					//saves staffdata
					$staffdata = array(
						'post_id' => $this->post_id,
						'user_id' => $userid,
						'description' => $staff['description'],
					);
					$staffid = $this->mpoststaff->saveorupdate($staffdata);
					
					//saves staff skills
					if($staffid){
						foreach( $this->postdata['staff']['skills'] as $staffskill ){
							$staffskilldata = array(
								'staff_id' => $staffid,
								'skill_id' => $staffskill,
							);						
							$this->mpoststaffskill->saveorupdate($staffskilldata);
						}
					}					
				}
			}
		} 		
		
	}
	/**----------------------------------------------------------------------------- SAVE ---------------------------------------------------------**/
}