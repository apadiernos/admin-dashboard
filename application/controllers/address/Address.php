
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Address Class
 *
 * @description Address Controller to get data
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Address extends Secure_Controller
{
	/**
	* @params int province id
	* @description get municiplaties by province
	* @return json format list of municipalities
	*/		
    public function getMunicpalities($province=null)
    {
		if( !$province ){
			$formData = $this->input->post();
			$province = $formData['provCode'];
		}
		
		if($province){
			$municipalitiesJson = getJson("address/refcitymun");
			$municipalities = $municipalitiesJson->RECORDS;
			$municipalitiesReturn = array();
			foreach($municipalities as $key => $municipality){
				if($municipality->provCode == $province){
					$municipalitiesReturn[$key] = new stdClass;
					$municipalitiesReturn[$key]->citymunCode = $municipality->citymunCode;
					$municipalitiesReturn[$key]->citymunDesc = $municipality->citymunDesc;
				}
			}
			if( $municipalitiesReturn ){
				$municipalitiesReturn[0]->citymunDesc = lang("choose");
				$municipalitiesReturn[0]->citymunCode = 0;
				echo json_encode($municipalitiesReturn);
			}
		}

	}

	/**
	* @params int municipality id
	* @description get municiplaties by province
	* @return json format list of municipalities
	*/		
    public function getBrgys($municipality=null)
    {
		$formData = $this->input->post();
		if( !$municipality ){
			$formData = $this->input->post();
			$municipality = $formData['citymunCode'];
		}
		
		if($municipality){
			$brgyJson = getJson("address/refbrgy");
			$brgys = $brgyJson->RECORDS;
			$brgysReturn = array();
			foreach($brgys as $key => $brgy){
				if($brgy->citymunCode == $municipality){
					$brgysReturn[$key] = new stdClass;
					$brgysReturn[$key]->brgyDesc = $brgy->brgyDesc;
					$brgysReturn[$key]->brgyCode = $brgy->brgyCode;
				}
			}
			if( $brgysReturn ){
				$brgysReturn[0]->brgyDesc = lang("choose");
				$brgysReturn[0]->brgyCode = 0;
				echo json_encode($brgysReturn);
			}
		}

	}	
}