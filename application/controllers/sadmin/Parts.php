
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sadmin Class
 *
 * @description Super Admin Controllers and renderers
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Parts extends Secure_Controller
{
	
	var $user_id = 0;

	var $pages = array();
	function __construct()
    {
    	parent::__construct();
		$this->load->model( array('sadmin/parts/mpartsbrands','sadmin/parts/mpartscategories') );
		$this->user_id = $this->session->userdata('user_id');
		$this->session->userdata('user_role');
		$this->validate_permission(SUPERADMIN);
		
 
    }
	
	/**
	* @description loads parts module
	*/		
    public function index()
    {
		$params = get_list_scripts();

		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('brand') => base_url('sadmin/parts/brands/list'),
			
		);
		//page title		
		$params['title'] = lang('title_brands_list');
			
		$this->render('sadmin/parts/brands/list',$params);		
    }
	
	/**----------------------------------------------------------------------------- BRANDS ---------------------------------------------------------**/
	/**
	* @description loads parts brands module pages
	*/		
    public function brands($page=null)
    {
		global $default_pages;
		if( !$page )
			$page = $this->uri->segment(4);
		
		if( $page == 'list'  ){
			$params = get_list_scripts();
	
			//breadcrumbs
			$params['breadcrumbs'] = array(
				lang('brand') => base_url('sadmin/parts/brands/list'),
				
			);		
		}else{
			$params = get_form_scripts();
			//get the brand by id
			$id = $this->uri->segment(5);
			if( $id ) $params['brand'] = $this->mpartsbrands->get(array('id'=>$id));	
		
			//breadcrumbs
			$params['breadcrumbs'] = array(
				lang('brand') => base_url('sadmin/parts/brands/list'),
				lang('addnew') => base_url('sadmin/parts/brands/add'),
				
			);		
		}
		//page title		
		$params['title'] = lang('title_brands_'.$page);
			
		$this->render('sadmin/parts/brands/'.$default_pages[$page],$params);
    }
	
	/**
	* @description remove parts brands by id
	*/		
    public function brands_remove()
    {
		$id = $this->uri->segment(4);
		$this->mpartsbrands->delete(array('id'=>$id));
    }
	
	/**
	* @description get brands via ajax
	*/			
	public function get_brands(){
		$formData = $this->input->post();
		
		//sorting columns
		$orderby = null;
		if( $formData['order'] ){
			$columns = array('brand.brand','name','brand.timecreated');
			$orderby["column"] = $columns[$formData['order'][0]['column']];
			$orderby["sort"] = $formData['order'][0]['dir'];
		}
		
		//pagination params
		$paging['limit'] = $formData['length'] ? $formData['length'] : 10;
		$paging['offset'] = $formData['start'] ? $formData['start'] : 0;
		
		//get records
		$brands = $this->get_brands_list($formData,$paging,$orderby);
				
		//total records
		if( $formData['search']['value']  ){
			$total = new stdClass;
			$total->total = count($brands);
		}else
			$total = $this->mpartsbrands->countAll();
		
		//dtTable params
		$dtData = new stdClass;
		$dtData->offset = $formData['draw'] ? $formData['draw'] : 0;
		$dtData->recordsTotal = $total->total;
		$dtData->recordsFiltered = $total->total;

		$data = array();
		foreach($brands as $key => $brand){
			$dataInfo = array();
			if($brand->nameEditor){
			  $dataInfo['Date Last Updated'] = format_time($brand->timeupdated);
			  $dataInfo['Last Updated By'] = $brand->nameEditor; 
			}
			$edit = '<a href="'.base_url('sadmin/parts/brands/edit/'.$brand->id).'" class="btn btn-success btn-cons"><i class="fa fa-pencil-square-o"></i>&nbsp;'.lang('edit').'</a>';
			$remove = '<a href="'.base_url('sadmin/parts/brands_remove/'.$brand->id).'" class="btn btn-danger btn-cons dt-row-remove"><i class="fa fa-times"></i>&nbsp;'.lang('remove').'</a>';
			$dataInfo[$edit] = $remove;			
			$data[$key] = array($brand->brand,$brand->name,format_time($brand->timecreated),json_encode($dataInfo));
			//$data[$key] = array($brand->brand,$brand->name,format_time($brand->timecreated));
		}	
		$dtData->dataList = $data;
		
		//dtTable accepted params
		$dtPrepared = $this->prepare_datatable_results($dtData);
		echo $dtPrepared;	
	}
	
	/**
	* @params array $formData form submission parameters to filter data
	* @params array $paging limit and offset for pagination
	* @description get list of records based on parameters
	*/		
	public function get_brands_list($formData=null,$paging=null,$orderby=null){
		$params = null;
		if( isset($formData['search']) ){
			//if( $formData['search']['from'] != "" )	 $params['date_'.$this->statuspage[$status].' >='] = date("Y-m-d",strtotime($formData['search']['from']));
			//if( $formData['search']['to'] != "" )	 $params['date_'.$this->statuspage[$status].' <='] = date("Y-m-d",strtotime($formData['search']['to']));			
			if( $formData['search']['value'] != "" ){
				$searchTerm  = array(
					'value'=>$formData['search']['value'],
					'operator_method'=>'or_like'
				);	
				$params['brand'] = $searchTerm;
				/**
				$params['amountwords'] = array(
					'value'=>$formData['search']['any'],
					'operator_method'=>'or_like',
					'group_start'=>true
				);
				$params['purpose'] = $params['remarks'] = $searchTerm;
				$params['amount '] = array(
					'value'=>$formData['search']['any'],
					'operator_method'=>'or_like',
					'group_end'=>true
				);
				**/
			}

		}
		//get brands
		return $this->mpartsbrands->getAll($params,$paging,$orderby);
	}
	/**----------------------------------------------------------------------------- BRANDS ---------------------------------------------------------**/
	
	
	/**----------------------------------------------------------------------------- CATEGORIES ---------------------------------------------------------**/
	/**
	* @description loads parts brands module pages
	*/		
    public function categories($page=null)
    {
		global $default_pages;
		if( !$page )
			$page = $this->uri->segment(4);
		
		if($page == 'list'  ){
			$params = get_list_scripts();
			
			//breadcrumbs
			$params['breadcrumbs'] = array(
				lang('category') => base_url('sadmin/parts/categories/list'),
				
			);				
		}else{
			$params = get_form_scripts();
			//get the category by id
			$id = $this->uri->segment(5);
			if( $id ) $params['category'] = $this->mpartscategories->get(array('id'=>$id));	
			
			//breadcrumbs
			$params['breadcrumbs'] = array(
				lang('category') => base_url('sadmin/parts/categories/list'),
				lang('addnew') => base_url('sadmin/parts/categories/add'),
				
			);				
		}
		$params['title'] = lang('title_categories_'.$page);
		$this->render('sadmin/parts/categories/'.$default_pages[$page],$params);
    }

	/**
	* @description remove parts brands by id
	*/		
    public function categories_remove()
    {
		$id = $this->uri->segment(4);
		$this->mpartscategories->delete(array('id'=>$id));
    }
	
	/**
	* @description get categories via ajax
	*/			
	public function get_categories(){
		$formData = $this->input->post();
		
		//sorting columns
		$orderby = null;
		if( $formData['order'] ){
			$columns = array('category.category','name','category.timecreated');
			$orderby["column"] = $columns[$formData['order'][0]['column']];
			$orderby["sort"] = $formData['order'][0]['dir'];
		}
		
		//pagination params
		$paging['limit'] = $formData['length'] ? $formData['length'] : 10;
		$paging['offset'] = $formData['start'] ? $formData['start'] : 0;
		
		//get records
		$categories = $this->get_categories_list($formData,$paging,$orderby);
				
		//total records
		if( $formData['search']['value']  ){
			$total = new stdClass;
			$total->total = count($categories);
		}else
			$total = $this->mpartscategories->countAll();
		
		//dtTable params
		$dtData = new stdClass;
		$dtData->offset = $formData['draw'] ? $formData['draw'] : 0;
		$dtData->recordsTotal = $total->total;
		$dtData->recordsFiltered = $total->total;

		$data = array();
		foreach($categories as $key => $category){
			$dataInfo = array();
			if($category->nameEditor){
			  $dataInfo['Date Last Updated'] = format_time($category->timeupdated);
			  $dataInfo['Last Updated By'] = $category->nameEditor; 
			}
			$edit = '<a href="'.base_url('sadmin/parts/categories/edit/'.$category->id).'" class="btn btn-success btn-cons"><i class="fa fa-pencil-square-o"></i>&nbsp;'.lang('edit').'</a>';
			$remove = '<a href="'.base_url('sadmin/parts/categories_remove/'.$category->id).'" class="btn btn-danger btn-cons dt-row-remove"><i class="fa fa-times"></i>&nbsp;'.lang('remove').'</a>';
			$dataInfo[$edit] = $remove;			
			$data[$key] = array($category->category,$category->name,format_time($category->timecreated),json_encode($dataInfo));
			//$data[$key] = array($brand->brand,$brand->name,format_time($brand->timecreated));
		}	
		$dtData->dataList = $data;
		
		//dtTable accepted params
		$dtPrepared = $this->prepare_datatable_results($dtData);
		echo $dtPrepared;	
	}
	
	/**
	* @params array $formData form submission parameters to filter data
	* @params array $paging limit and offset for pagination
	* @description get list of records based on parameters
	*/		
	public function get_categories_list($formData=null,$paging=null,$orderby=null){
		$params = null;
		if( isset($formData['search']) ){
			//if( $formData['search']['from'] != "" )	 $params['date_'.$this->statuspage[$status].' >='] = date("Y-m-d",strtotime($formData['search']['from']));
			//if( $formData['search']['to'] != "" )	 $params['date_'.$this->statuspage[$status].' <='] = date("Y-m-d",strtotime($formData['search']['to']));			
			if( $formData['search']['value'] != "" ){
				$searchTerm  = array(
					'value'=>$formData['search']['value'],
					'operator_method'=>'or_like'
				);	
				$params['category'] = $searchTerm;
			}

		}
		//get brands
		return $this->mpartscategories->getAll($params,$paging,$orderby);
	}	
	/**----------------------------------------------------------------------------- CATEGORIES ---------------------------------------------------------**/
	
	/**
	* @description saves parts categories and brands forms
	*/		
    public function save()
    {
		$formData = $this->input->post();

		//saves brands form data
		if( $formData['brands'] ){
			foreach( $formData['brands'] as $brands ){
				$this->mpartsbrands->saveorupdate($brands);
			}
			set_messages(IS_SUCCESS, lang('submit_success'));
			redirect('sadmin/parts/brands/add');
		}
		
		//saves categories form data
		if( $formData['categories'] ){
			foreach( $formData['categories'] as $categories ){
				$this->mpartscategories->saveorupdate($categories);
			}
			set_messages(IS_SUCCESS, lang('submit_success'));
			redirect('sadmin/parts/categories/add');	
		}	
		
    }

}