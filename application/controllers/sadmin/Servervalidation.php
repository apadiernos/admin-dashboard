
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Servervalidation Class
 *
 * @description Server Side request data validation
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Servervalidation extends Secure_Controller
{
	
	var $user_id = 0;
	function __construct()
    {
    	parent::__construct();
		
		$this->user_id = $this->session->userdata('user_id');
		
		$this->session->userdata('user_role');
		//$this->validate_permission(SUPERADMIN);
		
    }

	/**
	* @params string table table name
	* @params string model model name or path
	* @params string column column name
	* @params int/string value value
	* @description validate existing data on different models 
	* @return boolean if data is found or not
	*/		
	public function recordexists(){
		$formData = $this->input->post();

		//load model
		$this->load->model( $formData['model'] );
		
		//initiate get method
		$table = $formData['table'];
		$record = $this->$table->get(array($formData['column']=>$formData['value']));
		echo $record ? true : false;
	}	
	
	

	
}