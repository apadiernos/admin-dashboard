
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Services Class
 *
 * @description Services Controllers and renderers
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Services extends Secure_Controller
{
	
	var $user_id = 0;

	var $pages = array();
	function __construct()
    {
    	parent::__construct();
		$this->load->model( array('sadmin/mservicetypes') );
		$this->user_id = $this->session->userdata('user_id');
		$this->session->userdata('user_role');
		$this->validate_permission(SUPERADMIN);
		
 
    }
	
	/**
	* @description renders services list
	*/		
    public function index()
    {
		
		$params = get_list_scripts();

		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('service') => base_url('sadmin/services'),
			
		);
		//page title		
		$params['title'] = lang('title_services_list');
		$this->render('sadmin/services/list',$params);			
    }
	
	/**
	* @renders services add
	*/		
    public function add()
    {
		$params = get_form_scripts();

		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('service') => base_url('sadmin/services'),
			lang('addnew') => base_url('sadmin/services/add'),
			
		);
			
		$params['title'] = lang('title_services_add');
		$this->render('sadmin/services/save',$params);
    }
	
	/**
	* @renders services sdit
	*/		
    public function edit()
    {
		$params = get_form_scripts();

		//get the service by id
		$id = $this->uri->segment(4);
		if( $id ) $params['service'] = $this->mservicetypes->get(array('id'=>$id));	

		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('service') => base_url('sadmin/services'),
			lang('addnew') => base_url('sadmin/services/add'),
			
		);
		
		$params['title'] = lang('title_services_edit');
		$this->render('sadmin/services/save',$params);
    }

	/**
	* @description remove services by id
	*/		
    public function remove()
    {
		$id = $this->uri->segment(4);
		$this->mservicetypes->delete(array('id'=>$id));
    }
	
	/**
	* @description services via ajax
	*/			
	public function get_services(){
		$formData = $this->input->post();
		
		//sorting columns
		$orderby = null;
		if( $formData['order'] ){
			$columns = array('service.service','name','service.timecreated');
			$orderby["column"] = $columns[$formData['order'][0]['column']];
			$orderby["sort"] = $formData['order'][0]['dir'];
		}
		
		//pagination params
		$paging['limit'] = $formData['length'] ? $formData['length'] : 10;
		$paging['offset'] = $formData['start'] ? $formData['start'] : 0;
		
		//get records
		$services = $this->get_services_list($formData,$paging,$orderby);
				
		//total records
		if( $formData['search']['value']  ){
			$total = new stdClass;
			$total->total = count($services);
		}else
			$total = $this->mservicetypes->countAll();
		
		//dtTable params
		$dtData = new stdClass;
		$dtData->offset = $formData['draw'] ? $formData['draw'] : 0;
		$dtData->recordsTotal = $total->total;
		$dtData->recordsFiltered = $total->total;

		$data = array();
		foreach($services as $key => $service){
			$dataInfo = array();
			if($service->nameEditor){
			  $dataInfo['Date Last Updated'] = format_time($service->timeupdated);
			  $dataInfo['Last Updated By'] = $service->nameEditor; 
			}
			$edit = '<a href="'.base_url('sadmin/services/edit/'.$service->id).'" class="btn btn-success btn-cons"><i class="fa fa-pencil-square-o"></i>&nbsp;'.lang('edit').'</a>';
			$remove = '<a href="'.base_url('sadmin/services/remove/'.$service->id).'" class="btn btn-danger btn-cons dt-row-remove"><i class="fa fa-times"></i>&nbsp;'.lang('remove').'</a>';
			$dataInfo[$edit] = $remove;			
			$data[$key] = array($service->type,$service->name,format_time($service->timecreated),json_encode($dataInfo));
		}	
		$dtData->dataList = $data;
		
		//dtTable accepted params
		$dtPrepared = $this->prepare_datatable_results($dtData);
		echo $dtPrepared;	
	}
	
	/**
	* @params array $formData form submission parameters to filter data
	* @params array $paging limit and offset for pagination
	* @description get list of records based on parameters
	*/		
	public function get_services_list($formData=null,$paging=null,$orderby=null){
		$params = null;
		if( isset($formData['search']) ){
			//if( $formData['search']['from'] != "" )	 $params['date_'.$this->statuspage[$status].' >='] = date("Y-m-d",strtotime($formData['search']['from']));
			//if( $formData['search']['to'] != "" )	 $params['date_'.$this->statuspage[$status].' <='] = date("Y-m-d",strtotime($formData['search']['to']));			
			if( $formData['search']['value'] != "" ){
				$searchTerm  = array(
					'value'=>$formData['search']['value'],
					'operator_method'=>'or_like'
				);	
				$params['type'] = $searchTerm;
				/**
				$params['amountwords'] = array(
					'value'=>$formData['search']['any'],
					'operator_method'=>'or_like',
					'group_start'=>true
				);
				$params['purpose'] = $params['remarks'] = $searchTerm;
				$params['amount '] = array(
					'value'=>$formData['search']['any'],
					'operator_method'=>'or_like',
					'group_end'=>true
				);
				**/
			}

		}
		//get brands
		return $this->mservicetypes->getAll($params,$paging,$orderby);
	}	
	
	
	/**
	* @description save services records
	*/		
    public function save()
    {
		$formData = $this->input->post();

		//saves services form data
		if( $formData['services'] ){
			foreach( $formData['services'] as $services ){
				$this->mservicetypes->saveorupdate($services);
			}
			set_messages(IS_SUCCESS, lang('submit_success'));
			redirect('sadmin/services/add');
		}
	
		
    }
	
}