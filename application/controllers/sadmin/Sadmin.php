
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sadmin Class
 *
 * @description Super Admin Controllers and renderers
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Sadmin extends Secure_Controller
{
	
	var $user_id = 0;

	var $pages = array();
	function __construct()
    {
    	parent::__construct();
    	//$this->load->model( array('meformsrevisions','meforms','mactions','mattachment','mforms') );
		$this->user_id = $this->session->userdata('user_id');
		$this->session->userdata('user_role');
		$this->validate_permission(SUPERADMIN);
		
 
    }
	
	/**
	* @description loads dashboard module
	*/		
    public function index()
    {
		
		$params['js'] = array(
			'plugins/jquery-ui/' => array('jquery-ui-1.10.1.custom.min'),
			'plugins/bootstrap-datepicker/js/' => array('bootstrap-datepicker'),
			'plugins/jquery-block-ui/' => array('jqueryblockui'),
			'plugins/bootstrap-select2/' => array('select2.min'),
			'plugins/jquery-ricksaw-chart/js/' => array('raphael-min','d3.v2','rickshaw.min'),
			'plugins/jquery-morris-chart/' => array('morris.min'),
			'plugins/jquery-easy-pie-chart/js/' => array('jquery.easypiechart.min'),
			'plugins/jquery-slider/' => array('jquery.sidr.min'),
			'plugins/jquery-jvectormap/js/' => array('jquery-jvectormap-1.2.2.min','jquery-jvectormap-us-lcc-en'),
			'plugins/jquery-sparkline/' => array('jquery-sparkline'),
			'plugins/jquery-flot/' => array('jquery.flot.min','jquery.flot.animator.min'),
			'plugins/skycons/' => array('skycons'),
			'js/' => array('dashboard'),
		);
		$params['css'] = array(
			'plugins/gritter/css/' => array('jquery.gritter'),
			'plugins/bootstrap-datepicker/css/' => array('datepicker'),
			'plugins/jquery-ricksaw-chart/css/' => array('rickshaw'),
			'plugins/jquery-morris-chart/css/' => array('morris'),
			'plugins/bootstrap-select2/' => array('select2'),
			'plugins/jquery-jvectormap/css/' => array('jquery-jvectormap-1.2.2'),
		);
		
		$params['title'] = lang('title_dashboard');
		$this->render('sadmin/dashboard/v1',$params);
    }
	
	/**
	* @description loads client module pages
	*/		
    public function clients()
    {
		global $default_pages;
		$page = $this->uri->segment(2);

		
		if($page == 'list'  ){
			$params = get_list_scripts();
		}else{
			$params = get_form_scripts();
		}
		$params['title'] = lang('title_clients_'.$page);
		$this->render('sadmin/clients/'.$default_pages[$page],$params);
    }
		
}