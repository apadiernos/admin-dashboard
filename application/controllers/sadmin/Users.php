
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sadmin Class
 *
 * @description Users Controllers and renderers
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Users extends Secure_Controller
{
	
	var $user_id = 0;
	var $pages = array();
	var $provinces = "";
	function __construct()
    {
    	parent::__construct();
		$this->load->model( array('sadmin/muser') );
		$this->user_id = $this->session->userdata('user_id');
		$this->load->helper(array('url','html','form'));
		
		$this->config->load('images');
		$this->ppsize = $this->config->item("image_sizes")['square1'];
		
		//get provinces
		$this->provinces = new stdClass;
		$this->provinces = getJson("address/refprovince");
		
		$this->session->userdata('user_role');
		$this->validate_permission(SUPERADMIN);
		
    }
	
	/**
	* @description renders users list
	*/		
    public function index()
    {
		
		$params = get_list_scripts();

		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('user') => base_url('sadmin/users'),
			
		);
		//page title		
		$params['title'] = lang('title_users_list');
			
		$this->render('sadmin/users/list',$params);			
    }
	
	/**
	* @renders users add
	*/		
    public function add()
    {
		$params = get_form_scripts();
		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('user') => base_url('sadmin/users'),
			lang('addnew') => base_url('sadmin/users/add'),
			
		);
			
		$params['title'] = lang('title_users_add');
		$params['provinces'] = $this->provinces;
		$this->render('sadmin/users/save',$params);
    }
	
	/**
	* @renders users sdit
	*/		
    public function edit()
    {
		$params = get_form_scripts();

		//get the user by id
		$id = $this->uri->segment(4);
		if( $id ){
			$params['user'] = $this->muser->get(array('id'=>$id));	
			//get profile picture file directory
			$paramsPP = array(
				'file_raw_name' => $params['user']->ppfile_raw_name,
				'file_ext' => $params['user']->ppfile_ext,
				'file_timecreated' => $params['user']->pptimecreated,
				'size' => 'square1',
			);
			$params['user']->profilepicture = getFileDirectory($paramsPP);
		}
		//breadcrumbs
		$params['breadcrumbs'] = array(
			lang('user') => base_url('sadmin/users'),
			lang('addnew') => base_url('sadmin/users/add'),
			
		);
		

		$params['title'] = lang('title_users_edit');
		$params['provinces'] = $this->provinces;
		$this->render('sadmin/users/save',$params);
    }

	/**
	* @description remove users by id
	*/		
    public function remove()
    {
		$id = $this->uri->segment(4);
		$this->muser->delete(array('id'=>$id));
    }
	
	/**
	* @description get users via ajax
	*/			
	public function get_users(){
		$formData = $this->input->post();
		
		//sorting columns
		$orderby = null;
		if( $formData['order'] ){
			$columns = array('user.first_name','user.last_name','user.email_address','user.timecreated');
			$orderby["column"] = $columns[$formData['order'][0]['column']];
			$orderby["sort"] = $formData['order'][0]['dir'];
		}
		
		//pagination params
		$paging['limit'] = $formData['length'] ? $formData['length'] : 10;
		$paging['offset'] = $formData['start'] ? $formData['start'] : 0;
		
		//get records
		$users = $this->get_users_list($formData,$paging,$orderby);
				
		//total records
		if( $formData['search']['value']  ){
			$total = new stdClass;
			$total->total = count($users);
		}else
			$total = $this->muser->countAll();
		
		//dtTable params
		$dtData = new stdClass;
		$dtData->offset = $formData['draw'] ? $formData['draw'] : 0;
		$dtData->recordsTotal = $total->total;
		$dtData->recordsFiltered = $total->total;

		$data = array();
		foreach($users as $key => $user){
			$dataInfo = array();
			if($user->nameEditor){
			  $dataInfo['Date Last Updated'] = format_time($user->timeupdated);
			  $dataInfo['Date Last Updated'] = format_time($user->timeupdated);
			  $dataInfo['Last Updated By'] = $user->nameEditor; 
			}
			$dataInfo['Role'] = $user->role == SUPERADMIN ? lang("sadmin") : lang("cadmin");
			
			$edit = '<a href="'.base_url('sadmin/users/edit/'.$user->id).'" class="btn btn-success btn-cons"><i class="fa fa-pencil-square-o"></i>&nbsp;'.lang('edit').'</a>';
			$remove = '<a href="'.base_url('sadmin/users/remove/'.$user->id).'" class="btn btn-danger btn-cons dt-row-remove"><i class="fa fa-times"></i>&nbsp;'.lang('remove').'</a>';
			$dataInfo[$edit] = $remove;			
			$data[$key] = array($user->first_name,$user->last_name,$user->email_address,format_time($user->timecreated),json_encode($dataInfo));
		}	
		$dtData->dataList = $data;
		
		//dtTable accepted params
		$dtPrepared = $this->prepare_datatable_results($dtData);
		echo $dtPrepared;	
	}
	
	/**
	* @params array $formData form submission parameters to filter data
	* @params array $paging limit and offset for pagination
	* @description get list of records based on parameters
	*/		
	public function get_users_list($formData=null,$paging=null,$orderby=null){
		$params = null;
		if( isset($formData['search']) ){
			//if( $formData['search']['from'] != "" )	 $params['date_'.$this->statuspage[$status].' >='] = date("Y-m-d",strtotime($formData['search']['from']));
			//if( $formData['search']['to'] != "" )	 $params['date_'.$this->statuspage[$status].' <='] = date("Y-m-d",strtotime($formData['search']['to']));			
			if( $formData['search']['value'] != "" ){
				$searchTerm  = array(
					'value'=>$formData['search']['value'],
					'operator_method'=>'or_like'
				);	
				$params['first_name'] = $params['last_name'] = $params['email_address'] = $searchTerm;
				/**
				$params['amountwords'] = array(
					'value'=>$formData['search']['any'],
					'operator_method'=>'or_like',
					'group_start'=>true
				);
				$params['purpose'] = $params['remarks'] = $searchTerm;
				$params['amount '] = array(
					'value'=>$formData['search']['any'],
					'operator_method'=>'or_like',
					'group_end'=>true
				);
				**/
			}

		}
		//get brands
		return $this->muser->getAll($params,$paging,$orderby);
	}	
	
	
	/**
	* @description save users records
	*/		
    public function save()
    {
		global $_FILES;
		$formData = $this->input->post();
		//saves users form data
		if( $formData ){
			$formData['birthdate'] = $formData['birthdate'] ? strtotime($formData['birthdate']) : 0;
			$userid = $this->muser->saveorupdate($formData);
			set_messages(IS_SUCCESS, lang('submit_success'));
			
			if( $_FILES['single_image']['error'] == 0 ){
				//prepare profile picture upload params
				$params = array('module_id' => $userid, 'module' => 'users', 'module_ref'=>'userprofile', 'orphan'=>'0');
				$this->load->library('attachmentlib',$params);
				
				//check if profile picture exists
				if( $pp = $this->attachmentlib->get_files(1) ){
					$this->attachmentlib->remove_files($pp[0]->id);
				}
				//upload profile pic
				$this->attachmentlib->upload_files();	
			}
			redirect('sadmin/users/edit/'.$userid);
		}
	
		
    }
	
}