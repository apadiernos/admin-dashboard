<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Secure_Controller Class
 *
 * @description extends CI main controller for login users
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Secure_Controller extends CI_Controller
{
	var $user_id = 0;
	var $user_role = -1;
	var $user_name = "No User Logged In";
	
    public function __construct()
    {
        parent::__construct();
        $this->Authenticate();
    }
    
	/**
	* @description authenticate loggeid in user if not redirect to login page
	*/	
    private function Authenticate()
    {
    	$this->user_id = $this->session->userdata('user_id');
    	$this->user_name = $this->session->userdata('user_name');
    	$this->user_role = $this->session->userdata('user_role');
		
    	if ($this->user_id == NULL OR $this->user_id == 0) {
			redirect('login');
    	}
    }
	
	/**
	 * @params int $role_identifier
	 * @description validate web access permission via role. Log out user if access permission error.
	 */		
	public function validate_permission($role_identifier)
	{
		if( $role_identifier != $this->session->userdata('user_role') ){
			set_messages(IS_ERROR, lang('user_role_error'));
			redirect('logout');
		}		
	}
	
	/**
	 * @params string $view view render page
	 * @params array/stdClass $params parameters used in view
	 * @description render viewpage including default header and footer
	 */		
	public function render($view,$params=null)
	{
		$this->setheader($params);
		$this->load->view($view,$params);
		$this->setfooter($params);
	}
	
	/**
	 * @params array/stdClass $params parameters used in view
	 * @description render default header page
	 */		
	private function setheader($params=null)
	{
		$this->load->view('layout/header',$params);
	}

	/**
	 * @params array/stdClass $params parameters used in view
	 * @description render default footer page
	 */		
	private function setfooter($params=null)
	{
		$this->load->view('layout/footer',$params);
	}	
	
	/**
	 * @params stdClass $tableParams for datatable json result
	 * @description prepare json response for datatable
	 */			
	public function prepare_datatable_results($tableParams){
		$response = array(
			"draw"=>$tableParams->offset,
			"recordsTotal"=>$tableParams->recordsTotal,
			"recordsFiltered"=>$tableParams->recordsFiltered,
		);	
		$response["data"] = $tableParams->dataList;
		return json_encode($response);
	}	
}

function di($data){	
	print "<pre>";	print_r($data);	print "</pre>";
}