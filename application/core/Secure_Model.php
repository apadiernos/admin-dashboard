<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Secure_Model Class
 *
 * @description extends CI main controller for login users
 * @package    controller
 * @copyright  2016 Alan Padiernos
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Secure_Model extends CI_Model
{
	
	public $dbpostcategories = 'post_categories';	
	
	public $dbpost = 'posts';	
	public $dbposthasstaff = 'post_has_staff';	
	public $dbstaffhasskills = 'staff_has_skills';	
	public $dbposthasservices = 'post_has_services';	
	public $dbposthasparts = 'post_has_parts';	
	public $dbposthasowners = 'post_has_owners';	
	public $dbposthaslocations = 'post_has_locations';	
	public $dbposthascontactinfos = 'post_has_contactinfos';	
	
	
	public $dbUser = 'user';
	public $dbSkills = 'skills';
	public $dbServicetypes = 'service_types';
	public $dbPartscategories = 'parts_categories';
	public $dbPartsbrands = 'parts_brands';
	public $dbAttachment = 'attachments';
	
	public $user_id = 0;
	
	public function __construct()
    {
        $this->user_id = $this->session->userdata('user_id');
		parent::__construct();
    }	
	
	public function getQuery($prefix=null,$wheres=null,$paging=null,$orderby=null)
	{
		
		//join user to get the creator
		$this->db->join($this->dbUser.' user', 'user.id = '.$prefix.'.createdby');
		
		//join user to get the editor
		$this->db->join($this->dbUser.' userUp', 'userUp.id = '.$prefix.'.updatedby','left');
		
		if($wheres){
			foreach($wheres as $column => $value){
				if( !is_int($column) ){
					$column = strpos($column, '.') !== false ? $column : $prefix.'.'.$column;
					if( is_array($value) ){
						$operator_method = $value['operator_method'];
						isset($value['group_start']) ? $this->db->group_start() : null;
						
						$this->db->$operator_method($column, $value['value']);
						
						isset($value['group_end']) ? $this->db->group_end() : null;
					}else
						$this->db->where($column, $value);
				}else{
					if( is_array($value) ){
						foreach($value as $colname => $colval){
							if( is_array($colval) ){
								$operator_method = $colval['operator_method'];
								isset($colval['group_start']) ? $this->db->group_start() : null;
						
								$this->db->$operator_method('post.'.$colname, $colval['value']);
								
								isset($colval['group_end']) ? $this->db->group_end() : null;
							}else
								$this->db->where('post.'.$colname, $colval);
						}
					}else{
						$column = strpos($column, '.') !== false ? $column : $prefix.'.'.$column;
						$this->db->where($column, $value);
					}
				}
			}
		}	
		$this->db->group_by('id'); 
		
		//pagination
		if($paging) $this->db->limit($paging['limit'], $paging['offset']);
		
		//orderby
		if($orderby) $this->db->order_by($orderby["column"], $orderby["sort"]);		
	}
}