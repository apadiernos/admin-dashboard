<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<title><?php  echo lang('site_title'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta content="" name="description" />
	<meta content="" name="author" />






	<!-- BEGIN PLUGIN CSS -->
	<link href="<?php echo base_url();?>/webarch/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo base_url();?>/webarch/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>/webarch/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>/webarch/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>/webarch/assets/plugins/animate.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>/webarch/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css"/>
	<!-- END PLUGIN CSS -->
	<!-- BEGIN CORE CSS FRAMEWORK -->
	<link href="<?php echo base_url();?>/webarch/webarch/css/webarch.css" rel="stylesheet" type="text/css"/>
	<!-- END CORE CSS FRAMEWORK -->
</head>
		
<body class="error-body no-top">