       <?php
			global $role_constants;
			$dashhome = $role_constants[$this->session->userdata('user_role')]['dashboard'];
	  ?>     
	  <ul>
		<li class="start"> 
          <a href="<?php echo base_url($dashhome); ?>"> <i class="icon-custom-home"></i> <span class="title"><?php  echo lang('nav_dashboard'); ?></span> <span class=" badge badge-disable pull-right ">203</span>
          </a> 
        </li>			
		<li > 
          <a href="email.html"> <i class="fa fa-envelope"></i> <span class="title"><?php  echo lang('nav_email'); ?></span> <span class=" badge badge-disable pull-right ">203</span>
          </a> 
        </li>		
		
		
		<!-- posts -->
		<li > <a href="javascript:;"> <i class="fa fa-file-text"></i> <span class="title"><?php  echo lang('nav_posts'); ?></span> <span class=" arrow" ></span> </a>
  		    <ul class="sub-menu">
              <li > <a href="<?php echo base_url('post/add'); ?>"> <?php  echo lang('nav_add_new'); ?> </a> </li>
              <li class=""> <a href="<?php echo base_url('post'); ?>"> <?php  echo lang('nav_listall'); ?> <span class=" badge badge-important pull-right m-r-30">3</span></a></li>
				<!-- Posts Categories -->
				<li > <a href="javascript:;"> <i class="fa fa-filter"></i> <span class="title"><?php  echo lang('nav_parts_categories'); ?></span> <span class=" arrow" ></span> </a>
				  <ul class="sub-menu">
					  <li > <a href="<?php echo base_url('sadmin/post/categories/add'); ?>"> <?php  echo lang('nav_add_new'); ?> </a> </li>
					  <li class=""> <a href="<?php echo base_url('sadmin/post/categories'); ?>"> <?php  echo lang('nav_listall'); ?> <span class=" badge badge-important pull-right m-r-30">3</span></a></li>
				  </ul>
				</li>            
			</ul>
		</li>	

		<!-- Parts -->
		<li > <a href="javascript:;"> <i class="fa fa-barcode"></i> <span class="title"><?php  echo lang('nav_parts'); ?></span> <span class=" arrow" ></span> </a>    
			<ul class="sub-menu">
				
				<!-- Parts Brands -->
				<li > <a href="javascript:;"> <i class="fa fa-copyright"></i> <span class="title"><?php  echo lang('nav_parts_brands'); ?></span> <span class=" arrow" ></span> </a>
				  <ul class="sub-menu">
					  <li > <a href="<?php echo base_url('sadmin/parts/brands/add'); ?>"> <?php  echo lang('nav_add_new'); ?> </a> </li>
					  <li class=""> <a href="<?php echo base_url('sadmin/parts/brands/list'); ?>"> <?php  echo lang('nav_listall'); ?> <span class=" badge badge-important pull-right m-r-30">3</span></a></li>
				  </ul>
				</li>			

				<!-- Parts Categories -->
				<li > <a href="javascript:;"> <i class="fa fa-tags"></i> <span class="title"><?php  echo lang('nav_parts_categories'); ?></span> <span class=" arrow" ></span> </a>
				  <ul class="sub-menu">
					  <li > <a href="<?php echo base_url('sadmin/parts/categories/add'); ?>"> <?php  echo lang('nav_add_new'); ?> </a> </li>
					  <li class=""> <a href="<?php echo base_url('sadmin/parts/categories/list'); ?>"> <?php  echo lang('nav_listall'); ?> <span class=" badge badge-important pull-right m-r-30">3</span></a></li>
				  </ul>
				</li>
				
            </ul>
		</li>	

		<!-- Skills -->
		<li > <a href="javascript:;"> <i class="fa fa-id-card"></i> <span class="title"><?php  echo lang('nav_skills'); ?></span> <span class=" arrow" ></span> </a>    
			<ul class="sub-menu">
              <li > <a href="<?php echo base_url('sadmin/skills/add'); ?>"> <?php  echo lang('nav_add_new'); ?> </a> </li>
              <li class=""> <a href="<?php echo base_url('sadmin/skills'); ?> "> <?php  echo lang('nav_listall'); ?> <span class=" badge badge-important pull-right m-r-30">3</span></a></li>
            </ul>
		</li>	

		<!-- Services -->
		<li > <a href="javascript:;"> <i class="fa fa-wrench"></i> <span class="title"><?php  echo lang('nav_services'); ?></span> <span class=" arrow" ></span> </a>    
			<ul class="sub-menu">
              <li > <a href="<?php echo base_url('sadmin/services/add'); ?>"> <?php  echo lang('nav_add_new'); ?> </a> </li>
              <li class=""> <a href="<?php echo base_url('sadmin/services'); ?>"> <?php  echo lang('nav_listall'); ?> <span class=" badge badge-important pull-right m-r-30">3</span></a></li>
            </ul>
		</li>	

		<!-- Users -->
		<li > <a href="javascript:;"> <i class="fa fa-users"></i> <span class="title"><?php  echo lang('nav_users'); ?></span> <span class=" arrow" ></span> </a>
  		    <ul class="sub-menu">
              <li > <a href="<?php echo base_url('sadmin/users/add'); ?>"> <?php  echo lang('nav_add_new'); ?> </a> </li>
              <li class=""> <a href="<?php echo base_url('sadmin/users'); ?>"> <?php  echo lang('nav_listall'); ?> <span class=" badge badge-important pull-right m-r-30">3</span></a></li>
            </ul>
		</li>
		
		<!-- Configuration -->
		<li > <a href="javascript:;"> <i class="fa fa-cog"></i> <span class="title"><?php  echo lang('nav_settings'); ?></span> <span class=" arrow" ></span> </a>    
			<ul class="sub-menu">
              <li > <a href="dashboard_v1.html"> <?php  echo lang('nav_add_new'); ?> </a> </li>
              <li class=""> <a href="index.html "> <?php  echo lang('nav_listall'); ?> <span class=" badge badge-important pull-right m-r-30">3</span></a></li>
            </ul>
		</li>		
	</ul>