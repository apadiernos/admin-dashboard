<div id="form-brands-template" style="display:none">
	<?php brands_template(); ?>
</div>  
	  <div class="row">
		<?= form_open('sadmin/parts/save', array('id' => 'form_iconic_validation', 'class'=>'form-no-horizontal-spacing')); ?>		
        <div class="col-md-12">
          <div class="grid simple">		
				<div class="col-md-12">
					<br>
					<?php echo show_messages() ?>
					<br>
					<div class="form-brands">
						<?php if( $brand ): ?>
							<?php brands_template($brand,0); ?>
						<?php else: ?>
							<?php brands_template(); ?>
						<?php endif; ?>
					</div>
	  
				</div>
		  </div>	  
		</div>
			<div class="form-actions">
				<div class="pull-right">
				  <button class="btn btn-danger btn-cons" type="submit"><i class="fa fa-check"></i> &nbsp;<?=lang('save')?></button>
				  <a class="btn btn-white btn-cons" href="<?php echo base_url('sadmin/parts/brands/list'); ?>"><i class="fa fa-bars"></i>&nbsp; <?=lang('list')?></a>
				</div>
			</div>			
		<?= form_close(); ?>
	  </div>
  <?php
function brands_template($data=null,$index=0){
$brand_label = lang('brand');
echo <<<EOD

		<div class="row row-form-turnilyo">
			<div class="col-md-10">  
				<div class="row form-row">
				  <div class="col-md-12">
					<div class="input-group">
					  <span class="input-group-addon primary">				  
					  <span class="arrow"></span>
						<i class="fa fa-copyright"></i>
					  </span>
					  <input type="hidden" name="brands[$index][id]" value="$data->id" obj="brands" property="id"  >
					  <div class="input-with-icon  right"> 
						<i class=""></i>
					    <input value="$data->brand" name="brands[$index][brand]" obj="brands" property="brand" type="text" value="$data->brand" class="form-control required duplicate" placeholder="$brand_label" duplicate-model="sadmin/parts/mpartsbrands"  duplicate-column="brand" duplicate-table="mpartsbrands" >
					  </div>
					</div>
					<br>
				 </div>
				</div>	
			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-brands" data-cloned-template="#form-brands-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-brands"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
		</div>
EOD;


}
