<div id="form-categories-template" style="display:none">
	<?php categories_template(); ?>
</div>	
	  <div class="row">
		<?= form_open('sadmin/parts/save', array('id' => 'form_iconic_validation', 'class'=>'form-no-horizontal-spacing')); ?>	
        <div class="col-md-12">
          <div class="grid simple">		
				<div class="col-md-12">
					<br>
					<?php echo show_messages() ?>
					<br>
					<div class="form-categories">
						<?php if( $category ): ?>
							<?php categories_template($category,0); ?>
						<?php else: ?>
							<?php categories_template(); ?>
						<?php endif; ?>
					</div>
	  
				</div>
		  </div>	  
		</div>
			<div class="form-actions">
				<div class="pull-right">
				  <button class="btn btn-danger btn-cons" type="submit"><i class="icon-ok"></i> &nbsp;<?=lang('save')?></button>
				  <a class="btn btn-white btn-cons" href="<?php echo base_url('sadmin/parts/categories/list'); ?>"><i class="fa fa-bars"></i>&nbsp; <?=lang('list')?></a>
				</div>
			</div>			
		<?= form_close(); ?>
	  </div>
  
  <?php
function categories_template($data=null,$index=0){
$category_label = lang('category');
echo <<<EOD

		<div class="row row-form-turnilyo">
			<div class="col-md-10">  
				<div class="row form-row">
				  <div class="col-md-12">
					<div class="input-group">
					  <span class="input-group-addon primary">				  
					  <span class="arrow"></span>
						<i class="fa fa-tags"></i>
					  </span>
					  <input type="hidden" name="categories[$index][id]" value="$data->id" obj="categories" property="id"  >
					  <div class="input-with-icon  right"> 
						<i class=""></i>					  
						<input name="categories[$index][category]" obj="categories" property="category" type="text" value="$data->category"  class="form-control required duplicate" duplicate-model="sadmin/parts/mpartscategories"  duplicate-column="category" duplicate-table="mpartscategories"  placeholder="$category_label">
					  </div>
					</div>
					<br>
				 </div>
				</div>	
			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-categories" data-cloned-template="#form-categories-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-categories"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
		</div>
EOD;


}
