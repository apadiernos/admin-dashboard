	  <div class="row">
        <div class="col-md-12">
          <div class="grid simple">
			<br>
			<?php echo show_messages(); ?>
			<br>
            <div class="grid-body no-border">
			<?= form_open_multipart('sadmin/users/save', array('id' => 'form_iconic_validation', 'class'=>'form-no-horizontal-spacing')); ?>
              <input name="id" type="hidden" value="<?=$user->id?>" >
			  <div class="row column-seperation">
                <div class="col-md-6">				  
                  <h4><?=lang("basic_information")?></h4>  
				  <br>
                    <div class="row form-row">
					  <div class="col-md-12 single-image-upload">
							<div class="user-info-wrapper">
								<div class="profile-wrapper"> 
									<img class="bs-file-img" src="<?=$user->profilepicture ? $user->profilepicture : "http://turnilyo.org//webarch/assets/img/profiles/no-image.jpg"?>" alt="" data-src="<?=$user->profilepicture ? $user->profilepicture : "http://turnilyo.org//webarch/assets/img/profiles/no-image.jpg"?>" data-src-retina="http://turnilyo.org//webarch/assets/img/profiles/avatar2x.jpg" width="69" height="69">
								</div>	
							</div>

							<div class="input-group">
								<label class="input-group-btn">
									<span class="btn btn-primary">
										Browse… <input type="file" name="single_image" class="bs-file" style="display: none;" accept="image/*">
									</span>
								</label>
								<input type="text" class="form-control bs-file-input" readonly="">
								
							</div>
							<br>
					  </div>
					</div>
                    <div class="row form-row">
                      <div class="col-md-5">
						<div class="input-with-icon  right">
							<i class=""></i>
							<input name="first_name" id="first_name" type="text"  class="form-control required" value="<?=$user->first_name?>" placeholder="<?=lang("first_name")?>">
						</div>
					  </div>
                      <div class="col-md-7">
						<div class="input-with-icon  right">
							<i class=""></i>					  
							<input name="last_name" id="last_name" type="text"  class="form-control required" value="<?=$user->last_name?>" placeholder="<?=lang("last_name")?>">
						</div>
                      </div>
                    </div>
                    <div class="row form-row">
                      <div class="col-md-5">
						<div class="input-with-icon  right">
							<i class=""></i>
							<input name="username" id="username" type="text"  class="form-control required duplicate" value="<?=$user->username?>" placeholder="<?=lang("username")?>" duplicate-model="sadmin/muser"  duplicate-column="username" duplicate-table="muser" duplicate-message="<?=lang("username_taken")?>">
						</div>
					  </div>
                      <div class="col-md-7">
						<div class="input-with-icon  right">
							<i class=""></i>					  
							<input name="password" id="password" type="password"  class="form-control required" value="<?=$user->password?>" placeholder="<?=lang("password")?>">
						</div>
                      </div>
                    </div>					
                    <div class="row form-row">
                      <div class="col-md-8">
                        <div class="radio">
                          <input id="male" type="radio" name="gender" value="1" <?=$user->gender == 1 ? 'checked="checked"' : null?>>
                          <label for="male"><?=lang("male")?></label>
                          <input id="female" type="radio" name="gender" value="0" <?=$user->gender == 0 ? 'checked="checked"' : null?>>
                          <label for="female"><?=lang("female")?></label>
                        </div>
                      </div>
                    </div>
					
                    <div class="row form-row">
                      <div class="col-md-5">
                        <select name="role" id="role" class="select2 source required" style="width:100%" placeholder="<?=lang("role")?>" >
                          <option value="1" <?=$user->role == 1 ? 'selected="selected"' : null?>><?=lang("role_super_admin")?></option>
                          <option value="2" <?=$user->role == 2 ? 'selected="selected"' : null?>><?=lang("role_client_admin")?></option>
                        </select>
                      </div>
					  <div class="input-append success date col-md-7 col-lg-6 no-padding">
						<input type="text" placeholder="<?=lang("birthdate")?>" value="<?=format_time($user->birthdate)?>" class="form-control" id="birthdate" name="birthdate">
						<span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span> 
                      </div>
                    </div>
					
                    <div class="row form-row">
                      <div class="col-md-12">
						<div class="input-with-icon  right">
							<i class=""></i>					  
							<input name="email_address" id="email_address" type="email"  class="form-control required duplicate" value="<?=$user->email_address?>" placeholder="<?=lang("email_address")?>" duplicate-model="sadmin/muser"  duplicate-column="email_address" duplicate-table="muser" duplicate-message="<?=lang("email_taken")?>">
						</div>
					  </div>
                    </div>
                </div>
                <div class="col-md-6 location-row">
				
                  <h4><?=lang("postal_information")?></h4>
                  <br>
					<div class="row form-row">
                      <div class="col-md-12">
							<div class="input-with-icon  right">
								<i class=""></i>	
								<input name="address_line1" id="address_line1" type="text"  class="form-control required" value="<?=$user->address_line1?>" placeholder="<?=lang("address_line1")?>">
							</div>
					  </div>
                    </div>
                    <div class="row form-row">
                      <div class="col-md-12">
                        <input name="address_line2" id="address_line2" type="text"  class="form-control" value="<?=$user->address_line2?>" placeholder="<?=lang("address_line2")?>">
                      </div>
                    </div>	
					
                    <div class="row form-row">
                      <div class="col-md-6">
						<? $provinces = $provinces->RECORDS; ?>
						<select name="province" style="width:100%" id="province" type="text"  class="source province" placeholder="<?=lang("province")?>" >
							<option value="0"><?=lang("choose")?></option>
							<? foreach( $provinces as $province ): ?>
								 <option value="<?=$province->provCode?>" <?=$user->province == $province->provCode ? 'selected="selected"' : null?>><?=$province->provDesc?></option>
							<? endforeach; ?>
						</select>
                      </div>
                      <div class="col-md-6">
                        <select name="city_municipality" style="width:100%" id="city_municipality" default="<?=$user->city_municipality?>" type="text"  class="city_municipality" placeholder="<?=lang("city_municipality")?>">
						</select>
                      </div>						  
                    </div>
									
                    <div class="row form-row">
				
                      <div class="col-md-6">
                        <select name="brgy_district" style="width:100%" id="brgy_district" default="<?=$user->brgy_district?>" type="text"  class="brgy_district" placeholder="<?=lang("brgy_district")?>">
						</select>
                      </div>
                      <div class="col-md-6">
                        <input name="mobile_number" id="mobile_number" type="text"  class="form-control" value="<?=$user->mobile_number?>" placeholder="<?=lang("mobile_number")?>">
                      </div>					  
                    </div>

             		 
                </div>
              </div>
				
				<div class="form-actions">
					<div class="pull-right">
					  <button class="btn btn-danger btn-cons" type="submit"><i class="fa fa-check"></i> &nbsp;<?=lang('save')?></button>
					  <a class="btn btn-white btn-cons" href="<?php echo base_url('sadmin/users'); ?>"><i class="fa fa-bars"></i>&nbsp; <?=lang('list')?></a>
					</div>
				  </div>
				  
			<?= form_close(); ?>
            </div>
          </div>
        </div>
      </div>
	  <style>
	  .single-image-upload .user-info-wrapper {
		  margin: 0;
	  }	  
	  .single-image-upload .user-info-wrapper .profile-wrapper {
		   margin: 0 20px 20px 20px;
	  }
	  </style>