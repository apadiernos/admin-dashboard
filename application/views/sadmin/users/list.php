<div class="grid-body ">
  <table class="table table-striped" id="listdrop" url="<?=BASE_URL?>sadmin/users/get_users" >
	<thead>
	  <tr>
		<th><?=lang('first_name')?></th>
		<th><?=lang('last_name')?></th>
		<th><?=lang('email_address')?></th>
		<th><?=lang('datecreated')?></th>
	  </tr>
	</thead>
	<tbody>
	</tbody>
	<tfoot>
	  <tr>
		<th><?=lang('first_name')?></th>
		<th><?=lang('last_name')?></th>
		<th><?=lang('email_address')?></th>
		<th><?=lang('datecreated')?></th>
	  </tr>
	</tfoot>	
  </table>
</div>