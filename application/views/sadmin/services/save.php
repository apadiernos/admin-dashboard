<div id="form-services-template" style="display:none">
	<?php services_template(); ?>
</div>	  
	  <div class="row">
	   <?= form_open('sadmin/services/save', array('id' => 'form_iconic_validation', 'class'=>'form-no-horizontal-spacing')); ?>
        <div class="col-md-12">
          <div class="grid simple">		
				<div class="col-md-12">
					<br>
					<?php echo show_messages() ?>
					<br>
					<div class="form-services">
						<?php if( $service ): ?>
							<?php services_template($service,$index); ?>
						<?php else: ?>
							<?php services_template(); ?>
						<?php endif; ?>
					</div>
	  
				</div>
		  </div>	  
		</div>
			<div class="form-actions">
				<div class="pull-right">
				  <button class="btn btn-danger btn-cons" type="submit"><i class="fa fa-check"></i> &nbsp;<?=lang('save')?></button>
				  <a class="btn btn-white btn-cons" href="<?php echo base_url('sadmin/services'); ?>"><i class="fa fa-bars"></i>&nbsp; <?=lang('list')?></a>
				</div>
			</div>		
		<?= form_close(); ?>
	  </div>
  
  <?php
function services_template($data=null,$index=0){
$service_label = lang('service');
echo <<<EOD

		<div class="row row-form-turnilyo">
			<div class="col-md-10">  
				<div class="row form-row">
				  <div class="col-md-12">
					<div class="input-group">
					  <span class="input-group-addon primary">				  
					  <span class="arrow"></span>
						<i class="fa fa-copyright"></i>
					  </span>
					  <input type="hidden" name="services[$index][id]" value="$data->id" obj="services" property="id"  >
					  <div class="input-with-icon  right"> 
						<i class=""></i>
					    <input value="$data->type" name="services[$index][type]" obj="services" property="type" type="text" class="form-control required duplicate" duplicate-model="sadmin/mservicetypes"  duplicate-column="type" duplicate-table="mservicetypes"  placeholder="$service_label" >
					  </div>
					</div>
					<br>
				 </div>
				</div>	
			</div>		
			<div class="col-md-2">
				<div class="btn-group btn-group" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-services" data-cloned-template="#form-services-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-services"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
		</div>
EOD;


}
