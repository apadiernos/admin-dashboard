
<div class="grid-body ">
  <table class="table table-striped" id="listdrop" url="<?=BASE_URL?>sadmin/services/get_services" >
	<thead>
	  <tr>
		<th><?=lang('service')?></th>
		<th data-sort="disable"><?=lang('createdby')?></th>
		<th><?=lang('datecreated')?></th>
	  </tr>
	</thead>
	<tbody>
	</tbody>
	<tfoot>
	  <tr>
		<th><?=lang('service')?></th>
		<th data-sort="disable"><?=lang('createdby')?></th>
		<th><?=lang('datecreated')?></th>
	  </tr>
	</tfoot>	
  </table>
</div>