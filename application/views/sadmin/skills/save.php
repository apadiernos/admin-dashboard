<div id="form-skills-template" style="display:none">
	<?php skills_template(); ?>
</div>	  
	 <div class="row">
	   <?= form_open('sadmin/skills/save', array('id' => 'form_iconic_validation', 'class'=>'form-no-horizontal-spacing')); ?>	 
        <div class="col-md-12">
          <div class="grid simple">		
				<div class="col-md-12">
					<br>
					<?php echo show_messages() ?>
					<br>
					<div class="form-skills">
						<?php if( $skill ): ?>
							<?php skills_template($skill,$index); ?>
						<?php else: ?>
							<?php skills_template(); ?>
						<?php endif; ?>
					</div>
	  
				</div>
		  </div>	  
		</div>
			<div class="form-actions">
				<div class="pull-right">
				  <button class="btn btn-danger btn-cons" type="submit"><i class="fa fa-check"></i> &nbsp;<?=lang('save')?></button>
				  <a class="btn btn-white btn-cons" href="<?php echo base_url('sadmin/skills'); ?>"><i class="fa fa-bars"></i>&nbsp; <?=lang('list')?></a>
				</div>
			</div>			
		<?= form_close(); ?>
	  </div>
  
  <?php
function skills_template($data=null,$index=0){
$skill_label = lang('skill');

echo <<<EOD

		<div class="row row-form-turnilyo">
			<div class="col-md-10">  
				<div class="row form-row">
				  <div class="col-md-12">
					<div class="input-group">
					  <span class="input-group-addon primary">				  
					  <span class="arrow"></span>
						<i class="fa fa-copyright"></i>
					  </span>
					  <input type="hidden" name="skills[$index][id]" value="$data->id" obj="skills" property="id"  >
					  <div class="input-with-icon  right"> 
						<i class=""></i>
					    <input value="$data->skill" name="skills[$index][skill]" obj="skills" property="skill" type="text" value="$data->skill" class="form-control required duplicate" duplicate-model="sadmin/mskills"  duplicate-column="skill" duplicate-table="mskills"  placeholder="$skill_label" >
					  </div>
					</div>
					<br>
				 </div>
				</div>	
			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-skills" data-cloned-template="#form-skills-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-skills"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
		</div>
EOD;


}
