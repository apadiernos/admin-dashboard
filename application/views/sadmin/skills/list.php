
<div class="grid-body ">
  <table class="table table-striped" id="listdrop" url="<?=BASE_URL?>sadmin/skills/get_skills" >
	<thead>
	  <tr>
		<th><?=lang('skill')?></th>
		<th data-sort="disable"><?=lang('createdby')?></th>
		<th><?=lang('datecreated')?></th>
	  </tr>
	</thead>
	<tbody>
	</tbody>
	<tfoot>
	  <tr>
		<th><?=lang('skill')?></th>
		<th data-sort="disable"><?=lang('createdby')?></th>
		<th><?=lang('datecreated')?></th>
	  </tr>
	</tfoot>	
  </table>
</div>