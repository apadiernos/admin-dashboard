<?php $provinces = $provinces->RECORDS; ?>
<div id="form-location-template" style="display:none">
	<?php locations_template($provinces); ?>
</div>

<div class="tab-pane active" id="post-location">
  <div class="row">
	<div class="col-md-12">
	  <h3><?=lang("shop")?> <span class="semi-bold"><?=lang("locations")?></span></h3>
	

	<div class="form-locations">
			<?php if( $locations ): ?>
				<?php foreach($locations as $index => $location ): ?>
					<?php locations_template($provinces,$location,$index); ?>
				<?php endforeach; ?>
			<?php else: ?>		
				<?php locations_template($provinces); ?>
			<?php endif; ?>
		</div>
	  
	</div>
  </div>
</div>

<?php
function locations_template($provinces,$data=null,$index=0){
	

$address_line1 = lang("address_line1");
$address_line2 = lang("address_line2");
$province = lang("province");
$city_municipality = lang("city_municipality");
$brgy_district = lang("brgy_district");
$choose = lang("choose");
echo '

		<div class="row b-b b-grey row-form-turnilyo">		
			<br>
			<div class="col-md-10">  
				<div class="row form-row">
				  <div class="col-md-12">
					<input value="'.$data->address_line1.'" name="location['.$index.'][address_line1]" obj="location" property="address_line1" type="text"  class="form-control" placeholder="'.$address_line1.'">
				  </div>
				</div>
				<div class="row form-row">
				  <div class="col-md-12">
					<input value="'.$data->address_line2.'" name="location['.$index.'][address_line2]" obj="location" property="address_line2" type="text"  class="form-control" placeholder="'.$address_line2.'">
				  </div>
				</div>					
				<div class="row form-row location-row">
				
                      <div class="col-md-4">
						<select name="location['.$index.'][province]" obj="location" defaultval="1" property="province" style="width:100%" id="province" type="text"  class="source province" placeholder="'.$province.'" >
							<option value="0">'.$choose.'</option>';
							
							
							foreach( $provinces as $province ){ 
								$selectedProvince = $data->province == $province->provCode ? 'selected="selected"' : null;
								echo '<option value="'.$province->provCode.'" '.$selectedProvince.'>'.$province->provDesc.'</option>';
							}							
														
echo '
						</select>
                      </div>
                      <div class="col-md-4">
                        <select name="location['.$index.'][city_municipality]" obj="location" property="city_municipality" style="width:100%" id="city_municipality" default="'.$data->city_municipality.'" type="text"  class="city_municipality" placeholder="'.$city_municipality.'">
						</select>
                      </div>
                      <div class="col-md-4">
                        <select name="location['.$index.'][brgy_district]" obj="location" property="brgy_district" style="width:100%" id="brgy_district" default="'.$data->brgy_district.'" type="text"  class="brgy_district" placeholder="'.$brgy_district.'">
						</select>
                      </div>					  
		
				</div>

			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-locations" data-cloned-template="#form-location-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-locations"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
		</div>
';


}