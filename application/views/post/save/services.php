<style>
.services-photos {
	min-height: 150px;
}
</style>

<div id="form-services-template" style="display:none">
	<?php services_template($servicetypes); ?>
</div>

<div class="tab-pane" id="post-services">
  <div class="row">
	<div class="col-md-12">
	  <h3><?=lang("shop")?> <span class="semi-bold"><?=lang("services")?> </span></h3>
		
		<div class="form-services">
			<?php if( $services ): ?>
				<?php foreach($services as $index => $service): ?>
					<?php services_template($servicetypes,$service,$index); ?>
				<?php endforeach; ?>
			<?php else: ?>
				<?php services_template($servicetypes); ?>
			<?php endif; ?>
		</div>
	  
	</div>
  </div>
</div>

<?php
function services_template($servicetypes,$data=null,$index=0){

//servicetypes
$parts_categories = "";	
foreach( $servicetypes as $servicetype ){ 
	//$selectedservicetype = $data->province == $province->provCode ? 'selected="selected"' : null;
	$service_types .= '<option value="'.$servicetype->id.'" '.$selectedservicetype.'>'.$servicetype->type.'</option>';
}	

//language
$choose = lang("choose");
$name = lang("name");
$price = lang("price");
$display_price = lang("display_price");
$brand = lang("brand");
$category = lang("category");
$description = lang("description");
$including_labor = lang("including_labor");
$service = lang("service");
$service_type_as_name = lang("service_type_as_name");

echo <<<EOD

		<div class="row b-b b-grey row-form-turnilyo">
			<br>
			<div class="col-md-10">
				<div class="row form-row">
					<div class="col-md-12">
						<div class="row form-row">
						  <div class="col-md-6">
							<input value="$data->name" name="services[$index][name]" obj="services" property="part" type="text"  class="form-control" placeholder="$name">
						  </div>
						  <div class="col-md-4">
							<select name="services[$index][service_type_id]" obj="services" property="service_type_id" style="width:100%" id="parts_brand_id" type="text"  class="source service_type_id" placeholder="$service" >
								<option value="0">$choose</option>
								$service_types							
							</select>							
						  </div>						  						  
						</div>
						<div class="row form-row">
						  <div class="col-md-2">
							<input value="$data->price" name="services[$index][price]" obj="services" property="price" type="text"  class="form-control" placeholder="$price">
						  </div>
						  <div class="col-md-4">
							  <input name="services[$index][price]" obj="services" property="price" id="checkbox2" type="checkbox" value="0" >
							   $display_price <br>
							   <input name="services[$index][labor]" obj="services" property="labor" id="checkbox2" type="checkbox" value="0" >
							   $including_labor
						  </div>
						  <div class="col-md-4">
							<input name="services[$index][is_service_type]" obj="services" property="is_service_type" id="checkbox2" type="checkbox" value="0" >
							    $service_type_as_name <br>
						  </div>
						</div>			
						<div class="row form-row">
						  <div class="col-md-12">
							<textarea name="services[$index][description]" obj="services" property="description" placeholder="$description" class="text-editor form-control" rows="10">$data->description</textarea>
						  </div>
						</div>
					</div>
									
				</div>
			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-services" data-cloned-template="#form-services-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-services"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
		</div>
EOD;


}