

<style>
.dropzone .dz-default.dz-message {
	background-image: none;
}
.staff-photos {
	min-height: 120px;
}
</style>

<div id="form-staff-template" style="display:none">
	<?php staff_template($staffskills); ?>
</div>

<div class="tab-pane" id="post-staff">
  <div class="row">
	<div class="col-md-12">
	  <h3>Shop <span class="semi-bold">staff</span></h3>
		<div class="form-staff">
			<?php if( $staff ): ?>
				<?php foreach($staff as $index => $staffUser): ?>
					<?php staff_template($staffskills,$staffUser,$index); ?>
				<?php endforeach; ?>
			<?php else: ?>
				<?php staff_template($staffskills); ?>
			<?php endif; ?>
		</div>
	  
	</div>
  </div>
</div>

<?php
function staff_template($staffskills,$data=null,$index=0){
$pic = $data->logo ? $data->logo : "http://turnilyo.org//webarch/assets/img/profiles/no-image.jpg";


//staffskills
$staff_skills = "";	
foreach( $staffskills as $staffskill ){ 
	//$selectedstaffskill = $data->province == $province->provCode ? 'selected="selected"' : null;
	$staff_skills .= '<option value="'.$staffskill->id.'" '.$selectedstaffskill.'>'.$staffskill->skill.'</option>';
}	

//language
$firstname = lang("first_name");
$lastname = lang("last_name");
$email_address = lang("email_address");
$username = lang("username");
$choose = lang("choose");
$description = lang("description");
	
echo <<<EOD

		<div class="row b-b b-grey row-form-turnilyo">
			<br>
			<div class="col-md-10">  
				<div class="row form-row">
				  <div class="col-md-3 single-image-upload">

					<div class="row form-row">
						<div class="user-info-wrapper">
							<div class="profile-wrapper"> 
								<img class="bs-file-img" src="$pic" alt="" data-src="$pic" data-src-retina="$pic" width="69" height="69">
							</div>	
						</div>
					</div>
					<div class="row form-row">
					  <div class="col-md-12">
						<div class="input-group">
							<label class="input-group-btn">
								<span class="btn btn-primary">
									Browse… <input type="file" name="staff[$index][single_image]" obj="staff" property="single_image" class="bs-file" style="display: none;" accept="image/*">
								</span>
							</label>
							<input type="text" class="form-control bs-file-input" readonly="">
							
						</div>
						<br>
					  </div>	
					</div>
							  
						
					  <div class="row form-row">
						  <div class="col-md-12">
							  <select class="source" value="$data->lastname" name="staff[$index][skills][]"  obj="staff" property="[skills][]" style="width:100%" multiple placeholder="$choose">
								$staff_skills
							  </select>
						  </div>
						  <br>
					  </div>
				  </div>
				  <div class="col-md-9">
					  <div class="row form-row">
						  <div class="col-md-4">
							<input value="$data->first_name" name="staff[$index][first_name]" obj="staff" property="first_name" type="text"  class="form-control" placeholder="$firstname">
						  </div>
						  <div class="col-md-8">
							<input value="$data->last_name" name="staff[$index][last_name]"  obj="staff" property="last_name" type="text"  class="form-control" placeholder="$lastname">
						  </div>
					  </div>
					<div class="row form-row">
					  <div class="col-md-6">
						<input value="$data->username" name="staff[$index][username]"  obj="staff" property="username" type="text"  class="form-control" placeholder="$username">
					  </div>					  
					  <div class="col-md-6">
						<input value="$data->email_address" name="staff[$index][email_address]"  obj="staff" property="email_address" type="email"  class="form-control" placeholder="$email_address">
					  </div>					  
				    </div>					  
					  <div class="row form-row">
						  <div class="col-md-12">
							<textarea name="staff[$index][description]" obj="staff" property="description" placeholder="$description" class="text-editor form-control" rows="10">$data->description</textarea>
						  </div>					  
					  </div>
				  </div>
				</div> 				
			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-staff" data-cloned-template="#form-staff-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-staff"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
			<br>
		</div>
EOD;


}