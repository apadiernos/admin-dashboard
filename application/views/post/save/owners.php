
<div id="form-owners-template" style="display:none">
	<?php owners_template(); ?>
</div>

<div class="tab-pane" id="post-owners">
  <div class="row">
	<div class="col-md-12">
	  <h3><?=lang("shop")?> <span class="semi-bold"><?=lang("owners")?></span></h3>
		<div class="form-owners">
			<?php if( $owners ): ?>
				<?php foreach($owners as $index => $owner): ?>
					<?php owners_template($owner,$index); ?>
				<?php endforeach; ?>
			<?php else: ?>
				<?php owners_template(); ?>
			<?php endif; ?>
		</div>
	  
	</div>
  </div>
</div>
<style>
.profile-pic .user-info-wrapper {
  margin: 0;
}	  
.profile-pic .user-info-wrapper .profile-wrapper {
   margin: 0 25%;
}
</style>
<?php
function owners_template($data=null,$index=0){
$pic = $data->logo ? $data->logo : "http://turnilyo.org//webarch/assets/img/profiles/no-image.jpg";
$firstname = lang("first_name");
$lastname = lang("last_name");
$email_address = lang("email_address");
$username = lang("username");
echo <<<EOD

		<div class="row b-b b-grey row-form-turnilyo">
			<br>
			<div class="col-md-10">  
				<div class="row form-row">
				  <div class="col-md-2 single-image-upload">
					<div class="row form-row">
						<div class="user-info-wrapper">
							<div class="profile-wrapper"> 
								<img class="bs-file-img" src="$pic" alt="" data-src="$pic" data-src-retina="$pic" width="69" height="69">
							</div>	
						</div>
					</div>
					<div class="row form-row">
						<div class="input-group">
							<label class="input-group-btn">
								<span class="btn btn-primary">
									Browse… <input type="file" name="owners[$index][single_image]" obj="owners" property="single_image" class="bs-file" style="display: none;" accept="image/*">
								</span>
							</label>
							<input type="text" class="form-control bs-file-input" readonly="">
							
						</div>
						<br>
					</div>
				  </div>
				  <div class="col-md-10">
					<div class="row form-row">
					  <div class="col-md-6">
						<input value="$data->first_name" name="owners[$index][first_name]" obj="owners" property="first_name" type="text"  class="form-control" placeholder="$firstname">
					  </div>
					  <div class="col-md-6">
						<input value="$data->last_name" name="owners[$index][last_name]"  obj="owners" property="last_name" type="text"  class="form-control" placeholder="$lastname">
					  </div>
					  <div class="col-md-6">
						<input value="$data->username" name="owners[$index][username]"  obj="owners" property="username" type="text"  class="form-control" placeholder="$username">
					  </div>					  
					  <div class="col-md-6">
						<input value="$data->email_address" name="owners[$index][email_address]"  obj="owners" property="email_address" type="email"  class="form-control" placeholder="$email_address">
					  </div>					  
					</div>
				  </div>
				</div> 				
			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-owners" data-cloned-template="#form-owners-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-owners"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
			<br>
		</div>
EOD;


}