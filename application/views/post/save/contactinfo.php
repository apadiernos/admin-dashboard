<div id="form-contactinfo-template" style="display:none">
	<?php contactinfo_template(); ?>
</div>

<div class="tab-pane" id="post-contactinfo">
  <div class="row">
	<div class="col-md-12">
	  <h3><?=lang("contact")?> <span class="semi-bold"><?=lang("information")?></span></h3>
		<div class="form-contactinfo">
			<?php if( $contactinfos ): ?>
				<?php foreach($contactinfos as $index => $contactinfo): ?>
					<?php contactinfo_template($contactinfo,$index); ?>
				<?php endforeach; ?>
			<?php else: ?>
				<?php contactinfo_template(); ?>
			<?php endif; ?>
		</div>
	  
	</div>
  </div>
</div>

<?php
function contactinfo_template($data=null,$index=0){
$fn = 'contactinfo_tags';
$contact = lang("contact");
echo <<<EOD

		<div class="row b-b b-grey row-form-turnilyo">
			<br>
			<div class="col-md-10">  
				<div class="row form-row">
				  <div class="col-md-8">
					<input value="$data->value" name="contactinfo[$index][value]" obj="contactinfo" property="value" type="text"  class="form-control" placeholder="$contact">
				  </div>
				  <div class="col-md-4">
					{$fn($data->tag,$index)}
				  </div>
				</div>	
			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-contactinfo" data-cloned-template="#form-contactinfo-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-contactinfo"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
			<br>
		</div>
EOD;


}

function contactinfo_tags($tag=null,$index=0){
	$arr_contacts = array(
		'Email' =>array(
			'gmail_email' => 'Gmail',
			'yahoo_email' => 'Yahoo',
			'ms_email' => 'Microsoft',
			'other_email' => 'Other',
		),
		'Phone/Mobile' =>array(
			'work_phone' => 'Work Phone',
			'home_phone' => 'Home Phone',
			'mobile_phone' => 'Mobile Phone',
			'other_phone' => 'Other',
		),
		'Social Media' =>array(
			'facebook_socialmedia' => 'Facebook',
			'twitter_socialmedia' => 'Twitter',
			'youtube_socialmedia' => 'Youtube',
			'snapchat_socialmedia' => 'Snapchat',
			'linkedin_socialmedia' => 'Linkedin',
			'other_socialmedia' => 'Other',
		),
	);
	$sel = '<select class="source" name="contactinfo['.$index.'][tag]" obj="contactinfo" property="tag" style="width:100%">';
		foreach( $arr_contacts as $contactGroup => $contacts ){
			$sel .=  '<optgroup label="'.$contactGroup.'">';
			foreach( $contacts as  $contactLabel => $contact ){
				$selected  = $tag == $contactLabel ? "selected='selected'" : null;
				$sel .=  '<option value="'.$contactLabel.'" '.$selected.'>'.$contact.'</option>';
			}
			$sel .=  '</optgroup>';
		}
	$sel .= '</select>';
	return $sel;
}