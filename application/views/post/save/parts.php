<style>
.parts-photos {
	min-height: 150px;
}
</style>

<div id="form-parts-template" style="display:none">
	<?php parts_template($partsdata); ?>
</div>

<div class="tab-pane" id="post-parts">
  <div class="row">
	<div class="col-md-12">
	  <h3><?=lang("parts")?> <span class="semi-bold">& </span> <?=lang("accessories")?></h3>
		
		<div class="form-parts">
			<?php if( $parts ): ?>
				<?php foreach($parts as $index => $part): ?>
					<?php parts_template($partsdata,$part,$index); ?>
				<?php endforeach; ?>
			<?php else: ?>
				<?php parts_template($partsdata); ?>
			<?php endif; ?>
		</div>
	  
	</div>
  </div>
</div>

<?php
function parts_template($partsdata,$data=null,$index=0){
	
//parts brands
$parts_brands = "";	
foreach( $partsdata->brands as $brand ){ 
	//$selectedpartsbrands = $data->province == $province->provCode ? 'selected="selected"' : null;
	$parts_brands .= '<option value="'.$brand->id.'" '.$selectedpartsbrands.'>'.$brand->brand.'</option>';
}

//parts categories
$parts_categories = "";	
foreach( $partsdata->categories as $category ){ 
	//$selectedpartscategories = $data->province == $province->provCode ? 'selected="selected"' : null;
	$parts_categories .= '<option value="'.$category->id.'" '.$selectedpartscategories.'>'.$category->category.'</option>';
}	

//language
$choose = lang("choose");
$name = lang("name");
$price = lang("price");
$display_price = lang("display_price");
$brand = lang("brand");
$category = lang("category");
$description = lang("description");
$photos = lang("photos");
	
echo <<<EOD

		<div class="row b-b b-grey row-form-turnilyo">
			<br>
			<div class="col-md-10">
				<div class="row form-row">
					<div class="col-md-12">
						<div class="row form-row">
						  <div class="col-md-6">
							<input value="$data->name" name="parts[$index][name]" obj="parts" property="part" type="text"  class="form-control" placeholder="$name">
						  </div>
						  <div class="col-md-2">
							<input value="$data->price" name="parts[$index][price]" obj="parts" property="price" type="text"  class="form-control" placeholder="$price">
						  </div>
						  <div class="col-md-4">
							  
							  <input name="parts[$index][price]" obj="parts" property="price" id="checkbox2" type="checkbox" value="1" >
							   $display_price
						  </div>
						</div>
						<div class="row form-row">
						  <div class="col-md-6">
							<select name="parts[$index][parts_brand_id]" obj="parts" property="parts_brand_id" style="width:100%" id="parts_brand_id" type="text"  class="source parts_brand_id" placeholder="$brand" >
								<option value="0">$choose</option>
								$parts_brands							
							</select>
						  </div>
						  <div class="col-md-6">
							<select name="parts[$index][parts_category_id]" obj="parts" property="parts_category_id" style="width:100%" id="parts_category_id" type="text"  class="source parts_category_id" placeholder="$category" >
								<option value="0">$choose</option>
								$parts_categories
							</select>
						  </div>				  
						</div>				
						<div class="row form-row">
						  <div class="col-md-12">
							<textarea name="parts[$index][description]" obj="parts" property="description" placeholder="$description" class="text-editor form-control" rows="10">$data->description</textarea>
						  </div>
						</div>
					</div>
					
					<div class="col-md-12">
					  <h4>$photos </h4>
					  
					  <div class="grid simple">
						<div class="grid-body no-border">
						  <div class="row-fluid">
							<div class="dropzone no-margin parts-photos" maxfiles="10" module_id="0" module="post" module_ref="parts">
							  <div class="fallback">
								
							  </div>
							</div>
						  </div>
						</div>
					  </div>					
					</div>	
					
				</div>
			</div>	
			<div class="col-md-2">
				<div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
					<button class="btn btn-primary add-row" target=".form-parts" data-cloned-template="#form-parts-template"><i class="fa fa-plus"></i></button>
					<button class="btn btn-danger delete-row" target=".form-parts"><i class="fa fa-minus"></i></button>
				</div>		
			</div>
			<div class="clearfix"></div>
		</div>
EOD;


}