
<div class="grid-body ">
  <table class="table table-striped" id="listdrop" url="<?=BASE_URL?>post/get_post" >
	<thead>
	  <tr>
		<th><?=lang('client')?></th>
		<th data-sort="disable"><?=lang('createdby')?></th>
		<th><?=lang('datecreated')?></th>
	  </tr>
	</thead>
	<tbody>
	</tbody>
	<tfoot>
	  <tr>
		<th><?=lang('client')?></th>
		<th data-sort="disable"><?=lang('createdby')?></th>
		<th><?=lang('datecreated')?></th>
	  </tr>
	</tfoot>	
  </table>
</div>