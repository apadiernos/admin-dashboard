      <?= form_open_multipart('post/save', array('id' => 'form_iconic_validation', 'class'=>'form-no-horizontal-spacing')); ?>
 			
	  <div class="row">
        <div class="col-md-12">
          <div class="grid simple">
            <div class="grid-body no-border">
				<input type="hidden" name="formsession" value="<?=$formsession?>">				             
			  <div class="row column-seperation">
                <div class="col-md-6">
                  <h4><?=lang("basic_information")?></h4>            
                    <div class="row form-row">
                      <div class="col-md-12">
							<div class="input-with-icon  right">
								<i class=""></i>					  
								<input name="client" id="client" type="text"  class="form-control required duplicate" value="<?=$post->client?>" duplicate-model="post/mpost"  duplicate-column="client" duplicate-table="mpost" duplicate-message="<?=lang("post_exists")?>" placeholder="<?=lang("client")?>">
							</div>                    
					  </div>
                    </div>            
                    <div class="row form-row">
                      <div class="col-md-12">
                        <textarea  name="description" placeholder="<?=lang("description")?>" class="text-editor form-control" rows="10"><?=$post->description?></textarea>
                      </div>
                    </div>

                </div>
                <div class="col-md-6">
				
                  <h4><?=lang("other_nformation")?></h4>
					
                    <div class="row form-row">
					  <div class="col-md-12 single-image-upload">
							<div class="user-info-wrapper">
								<div class="profile-wrapper"> 
									<img class="bs-file-img" src="<?=$post->logo ? $post->logo : "http://turnilyo.org//webarch/assets/img/profiles/no-image.jpg"?>" alt="" data-src="<?=$post->logo ? $post->logo : "http://turnilyo.org//webarch/assets/img/profiles/no-image.jpg"?>" data-src-retina="http://turnilyo.org//webarch/assets/img/profiles/avatar2x.jpg" width="69" height="69">
								</div>	
							</div>

							<div class="input-group">
								<label class="input-group-btn">
									<span class="btn btn-primary">
										Browse… <input type="file" name="single_image" class="bs-file" style="display: none;" accept="image/*">
									</span>
								</label>
								<input type="text" class="form-control bs-file-input" readonly="">

							</div>
							<h5><?=lang("company_logo")?></h5>
							<br>
					  </div>
					</div>
                </div>
			  </div>
			</div>
		  </div>	
        </div>
      </div>
	  
	  <div class="row">
        <div class="col-md-12">
          <div class="grid simple">		
				<div class="col-md-12">
				  <ul class="nav nav-tabs" role="tablist">
					<li class="active">
					  <a href="#post-location" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i> <?=lang("locations")?></a>
					</li>
					<li>
					  <a href="#post-contactinfo" role="tab" data-toggle="tab"><i class="fa fa-address-book"></i> <?=lang("contact_information")?></a>
					</li>
					<li>
					  <a href="#post-owners" role="tab" data-toggle="tab"><i class="fa fa-users"></i> <?=lang("owners")?></a>
					</li>
					<li>
					  <a href="#post-parts" role="tab" data-toggle="tab"><i class="fa fa-shopping-cart"></i>  <?=lang("parts_accessories")?></a>
					</li>
					<li>
					  <a href="#post-services" role="tab" data-toggle="tab"><i class="fa fa-wrench"></i>  <?=lang("services")?></a>
					</li>	
					<li>
					  <a href="#post-staff" role="tab" data-toggle="tab"><i class="fa fa-id-badge"></i>  <?=lang("staff")?></a>
					</li>					
				  </ul>
				  <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
				  <div class="tab-content">
				  
					<!-- POST LOCATIONS -->
					<?php $this->view('post/save/locations'); ?>
					
					<!-- POST CONTACT INFO -->
					<?php $this->view('post/save/contactinfo'); ?>	
					
					<!-- POST OWNERS -->
					<?php $this->view('post/save/owners'); ?>					

					<!-- POST PARTS AND ACCESSORIES -->	
					<?php $this->view('post/save/parts'); ?>	
					
					<!-- POST SERVICES -->
					<?php $this->view('post/save/services'); ?>		
					
					<!-- POST STAFF -->
					<?php $this->view('post/save/staff'); ?>					


				  </div>
				</div>				
              </div>
				<div class="form-actions">
					<div class="pull-right">
					  <button class="btn btn-danger btn-cons" type="submit"><i class="fa fa-check"></i> &nbsp;<?=lang('save')?></button>
					  <a class="btn btn-white btn-cons" href="<?php echo base_url('post'); ?>"><i class="fa fa-bars"></i>&nbsp; <?=lang('list')?></a>
					</div>
				  </div>
			
            </div>
          </div>	
			<?= form_close(); ?>
	  <style>
	  .single-image-upload .user-info-wrapper {
		  margin: 0;
	  }	  
	  .single-image-upload .user-info-wrapper .profile-wrapper {
		   margin: 0 20px 20px 20px;
	  }
	  </style>		  