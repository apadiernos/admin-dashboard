
	<div id="main-chat-wrapper" class="inner-content">
        <div class="chat-window-wrapper scroller scrollbar-dynamic" id="chat-users" >
			
			<!-- chat search--> 
			<?php $this->view('layout/chat/search'); ?>
			
			<!-- chat groups--> 
			<?php $this->view('layout/chat/groups'); ?>

			<!-- chat groups--> 
			<?php $this->view('layout/chat/online'); ?>
        </div>

		<div class="chat-window-wrapper" id="messages-wrapper" style="display:none">
			<!-- chat groups--> 
			<?php $this->view('layout/chat/window/search'); ?>
			
			<!-- chat groups--> 
			<?php $this->view('layout/chat/window/conversation'); ?>
			
			<div class="clearfix"></div>
        </div>		
    </div>
