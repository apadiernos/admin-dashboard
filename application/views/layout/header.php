<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta charset="utf-8" />
		<title><?php  echo lang('site_title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		
		<!-- BEGIN PLUGIN CSS -->
		<?php provision_scripts($css,'css'); ?>
		<!-- END PLUGIN CSS -->

		<!-- BEGIN PLUGIN CSS -->
		<link href="<?php echo base_url();?>/webarch/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
		<link href="<?php echo base_url();?>/webarch/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url();?>/webarch/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url();?>/webarch/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url();?>/webarch/assets/plugins/animate.min.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url();?>/webarch/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css"/>
		
		<!-- END PLUGIN CSS -->
		
		<!-- BEGIN CORE CSS FRAMEWORK -->
		<link href="<?php echo base_url();?>/webarch/webarch/css/webarch.css" rel="stylesheet" type="text/css"/>
		<!-- END CORE CSS FRAMEWORK -->
	</head>
	<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="" base_url="<?php echo base_url();?>">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
	<div class="header-seperation"> 
		<ul class="nav pull-left notifcation-center visible-xs visible-sm">	
		 <li class="dropdown"> 
		 	<a href="#main-menu" data-webarch="toggle-left-side"> 
		 		<div class="iconset top-menu-toggle-white"></div> 
		 	</a> 
		 </li>		 
		</ul>
		<!-- logo -->
		<?php $this->view('layout/header/logo'); ?>
		
		<!-- Home, Message and Toogle responsive -->
		<?php $this->view('layout/header/home'); ?>

      </div>
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav" > 
		<?php $this->view('layout/header/quick_nav'); ?>
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
   
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="page-container row">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar " id="main-menu">
	<?php $this->view('layout/sidebar'); ?>
  </div>
  <!-- END SIDEBAR -->	
    <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button"></button>
        <h3>Widget Settings</h3>
      </div>
      <div class="modal-body"> Widget settings form goes here </div>
    </div>
    <div class="clearfix"></div>
    <div class="content">
      
	  <!-- breadcrumb --> 
	  <?php $this->view('layout/header/breadcrumb'); ?>
