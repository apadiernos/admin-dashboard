
    
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
      <!-- BEGIN MINI-PROFILE -->
		<?php $this->view('layout/sidebar/mini_profile'); ?>
      <!-- END MINI-PROFILE -->
      
	  <!-- BEGIN SIDEBAR MENU -->
      <p class="menu-title">BROWSE <span class="pull-right"><a href="javascript:;"><i class="fa fa-refresh"></i></a></span></p>
      <?php
			global $role_constants;
			$navview = $role_constants[$this->session->userdata('user_role')]['view'];
	  ?>	  
	  <!-- sidebar navigation -->
	  <?php $this->view($navview.'/sidebar/navigation'); ?>
	  
	  <!-- sidebar widgets -->
	  <?php //$this->view('layout/sidebar/widgets'); ?>
	  

      <!-- END SIDEBAR MENU -->
    </div>
  </div>
  <a href="#" class="scrollup">Scroll</a>
  <div class="footer-widget">
    <div class="progress transparent progress-small no-radius no-margin">
      <div class="progress-bar progress-bar-success animate-progress-bar" data-percentage="79%" style="width: 79%;"></div>
    </div>
    <div class="pull-right">
      <div class="details-status"> <span class="animate-number" data-value="86" data-animation-duration="560">86</span>% </div>
      <a href="lockscreen.html"><i class="fa fa-power-off"></i></a></div>
