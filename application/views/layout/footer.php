				</div>
			</div>
			
			<!-- BEGIN CHAT --> 
			<div class="chat-window-wrapper">
				<?php $this->view('layout/chat'); ?>
			</div>
			<!-- END CHAT --> 
		<!-- END CONTAINER -->
		</div>

	<script src="<?php echo base_url();?>/webarch/assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
	<!-- BEGIN JS DEPENDECENCIES--> 
	<script src="<?php echo base_url();?>/webarch/assets/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>/webarch/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>/webarch/assets/plugins/jquery-block-ui/jqueryblockui.min.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>/webarch/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>/webarch/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>/webarch/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>/webarch/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>/webarch/assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
	<!-- END CORE JS DEPENDECENCIES--> 
	<!-- BEGIN CORE TEMPLATE JS --> 
	<script src="<?php echo base_url();?>/webarch/webarch/js/webarch.js" type="text/javascript"></script> 
	<script src="<?php echo base_url();?>/webarch/assets/js/chat.js" type="text/javascript"></script> 
	<!-- END CORE TEMPLATE JS --> 
	<!-- BEGIN PAGE LEVEL JS -->
	<?php provision_scripts($js,'js'); ?>
	<!-- END PAGE LEVEL PLUGINS   -->

	</body>
</html>
