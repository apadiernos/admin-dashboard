      <?php
			global $role_constants;
			$dashhome = $role_constants[$this->session->userdata('user_role')]['dashboard'];
	  ?>
	  <ul class="nav pull-right notifcation-center">	
	  
        <li class="dropdown hidden-xs hidden-sm"> 
        	<a href="<?php echo base_url($dashhome); ?>" class="dropdown-toggle active" data-toggle=""> 
        		<div class="iconset top-home"></div>
        	</a>
        </li>
        <li class="dropdown hidden-xs hidden-sm"> 
        	<a href="email.html" class="dropdown-toggle" > 
        		<div class="iconset top-messages"></div><span class="badge">2</span> 
        	</a>
        </li>
		<li class="dropdown visible-xs visible-sm"> 
			<a href="#" data-webarch="toggle-right-side">
				<div class="iconset top-chat-white "></div>
			</a> 
		</li>        
      </ul>