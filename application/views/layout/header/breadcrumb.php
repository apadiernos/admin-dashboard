<ul class="breadcrumb">
	<?
	$firstCrumb = array_kshift($breadcrumbs);
	?>
	<? foreach($firstCrumb as $breadcrumb => $linkbreadcrumb ): ?>
		<li>
		  <p><?=$breadcrumb?></p>
		</li>
	<? endforeach; ?>	
	<? foreach($breadcrumbs as $breadcrumb => $link ): ?>
		<li><a href="<?=$link?>" class="active"><?=$breadcrumb?></a> </li>
	<? endforeach; ?>
</ul>
<div class="page-title"> <a href="<?=$linkbreadcrumb?>"> <i class="icon-custom-left"></i> </a>
	<h3><?php echo $title; ?> </h3>
</div> 