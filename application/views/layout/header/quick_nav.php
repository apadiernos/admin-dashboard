  <!-- BEGIN TOP NAVIGATION MENU -->
  <div class="pull-left"> 
	<ul class="nav quick-section">
	  <li class="quicklinks"> <a href="#" class="" id="layout-condensed-toggle" >
		<div class="iconset top-menu-toggle-dark"></div>
		</a> </li>
	</ul>
	<ul class="nav quick-section">
	  <!-- reload -->	
	  <li class="quicklinks"> <a href="#" class="" >
		<div class="iconset top-reload"></div>
		</a>
	  </li>
	  <li class="quicklinks"> <span class="h-seperate"></span></li>
	  
	   <!-- grid -->
	  <li class="quicklinks"> <a href="#" class="" >
		<div class="iconset top-tiles"></div>
		</a> 
	  </li>
	  
	  <?php $this->view('layout/header/quick_nav/search'); ?>
	</ul>
  </div>
 <!-- END TOP NAVIGATION MENU -->
 
 
 <!-- BEGIN CHAT TOGGLER -->
  <div class="pull-right"> 
	<!-- chat/notifications -->
	<?php $this->view('layout/header/quick_nav/notifications'); ?>
	
	<!-- logged in user -->
	<?php $this->view('layout/header/quick_nav/user'); ?>
  </div>
   <!-- END CHAT TOGGLER -->