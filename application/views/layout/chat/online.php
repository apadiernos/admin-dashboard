<div class="side-widget fadeIn">
   <div class="side-widget-title">favourites</div>
   <div id="favourites-list">
	<div class="side-widget-content" >
		<div class="user-details-wrapper active" data-chat-status="online" data-chat-user-pic="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" data-user-name="Jane Smith">
			<div class="user-profile">
				<img src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" width="35" height="35">
			</div>
			<div class="user-details">
				<div class="user-name">
				Jane Smith
				</div>
				<div class="user-more">
				Hello you there?
				</div>
			</div>
			<div class="user-details-status-wrapper">
				<span class="badge badge-important">3</span>
			</div>
			<div class="user-details-count-wrapper">
				<div class="status-icon green"></div>
			</div>
			<div class="clearfix"></div>
		</div>	
		<div class="user-details-wrapper" data-chat-status="busy" data-chat-user-pic="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" data-user-name="David Nester">
			<div class="user-profile">
				<img src="<?php echo base_url();?>/webarch/assets/img/profiles/c.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/c.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/c2x.jpg" width="35" height="35">
			</div>
			<div class="user-details">
				<div class="user-name">
				David Nester
				</div>
				<div class="user-more">
				Busy, Do not disturb
				</div>
			</div>
			<div class="user-details-status-wrapper">
				<div class="clearfix"></div>
			</div>
			<div class="user-details-count-wrapper">
				<div class="status-icon red"></div>
			</div>
			<div class="clearfix"></div>
		</div>					
	</div>
	</div>
</div>
<div class="side-widget">
   <div class="side-widget-title">more friends</div>
	 <div class="side-widget-content" id="friends-list">
		<div class="user-details-wrapper" data-chat-status="online" data-chat-user-pic="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" data-user-name="Jane Smith">
			<div class="user-profile">
				<img src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" width="35" height="35">
			</div>
			<div class="user-details">
				<div class="user-name">
				Jane Smith
				</div>
				<div class="user-more">
				Hello you there?
				</div>
			</div>
			<div class="user-details-status-wrapper">

			</div>
			<div class="user-details-count-wrapper">
				<div class="status-icon green"></div>
			</div>
			<div class="clearfix"></div>
		</div>	
		<div class="user-details-wrapper" data-chat-status="busy" data-chat-user-pic="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" data-user-name="David Nester">
			<div class="user-profile">
				<img src="<?php echo base_url();?>/webarch/assets/img/profiles/h.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/h.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/h2x.jpg" width="35" height="35">
			</div>
			<div class="user-details">
				<div class="user-name">
				David Nester
				</div>
				<div class="user-more">
				Busy, Do not disturb
				</div>
			</div>
			<div class="user-details-status-wrapper">
				<div class="clearfix"></div>
			</div>
			<div class="user-details-count-wrapper">
				<div class="status-icon red"></div>
			</div>
			<div class="clearfix"></div>
		</div>		
		<div class="user-details-wrapper" data-chat-status="online" data-chat-user-pic="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" data-user-name="Jane Smith">
			<div class="user-profile">
				<img src="<?php echo base_url();?>/webarch/assets/img/profiles/c.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/c.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/c2x.jpg" width="35" height="35">
			</div>
			<div class="user-details">
				<div class="user-name">
				Jane Smith
				</div>
				<div class="user-more">
				Hello you there?
				</div>
			</div>
			<div class="user-details-status-wrapper">

			</div>
			<div class="user-details-count-wrapper">
				<div class="status-icon green"></div>
			</div>
			<div class="clearfix"></div>
		</div>	
		<div class="user-details-wrapper" data-chat-status="busy" data-chat-user-pic="assets/img/profiles/d.jpg" data-chat-user-pic-retina="assets/img/profiles/d2x.jpg" data-user-name="David Nester">
			<div class="user-profile">
				<img src="<?php echo base_url();?>/webarch/assets/img/profiles/h.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/h.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/h2x.jpg" width="35" height="35">
			</div>
			<div class="user-details">
				<div class="user-name">
				David Nester
				</div>
				<div class="user-more">
				Busy, Do not disturb
				</div>
			</div>
			<div class="user-details-status-wrapper">
				<div class="clearfix"></div>
			</div>
			<div class="user-details-count-wrapper">
				<div class="status-icon red"></div>
			</div>
			<div class="clearfix"></div>
		</div>				
	</div>		
</div>