<div class="chat-messages-header">
	<div class="status online"></div><span class="semi-bold">Jane Smith(Typing..)</span>
	<a href="#" class="chat-back"><i class="icon-custom-cross"></i></a>
</div>
<div class="chat-messages scrollbar-dynamic clearfix">
	<div class="inner-scroll-content clearfix">
	<div class="sent_time">Yesterday 11:25pm</div>
	<div class="user-details-wrapper " >
		<div class="user-profile">
			<img src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" width="35" height="35">
		</div>
		<div class="user-details">
		  <div class="bubble">	
				Hello, You there?
		   </div>
		</div>					
		<div class="clearfix"></div>
	   <div class="sent_time off">Yesterday 11:25pm</div>
	</div>		
	<div class="user-details-wrapper ">
		<div class="user-profile">
			<img src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" width="35" height="35">
		</div>
		<div class="user-details">
		  <div class="bubble">	
				How was the meeting?
		   </div>
		</div>					
		<div class="clearfix"></div>
		<div class="sent_time off">Yesterday 11:25pm</div>
	</div>
	<div class="user-details-wrapper ">
		<div class="user-profile">
			<img src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg"  alt="" data-src="<?php echo base_url();?>/webarch/assets/img/profiles/d.jpg" data-src-retina="<?php echo base_url();?>/webarch/assets/img/profiles/d2x.jpg" width="35" height="35">
		</div>
		<div class="user-details">
		  <div class="bubble">	
				Let me know when you free
		   </div>
		</div>					
		<div class="clearfix"></div>
		<div class="sent_time off">Yesterday 11:25pm</div>
	</div>
	<div class="sent_time ">Today 11:25pm</div>
	<div class="user-details-wrapper pull-right">
		<div class="user-details">
		  <div class="bubble sender">	
				Let me know when you free
		   </div>
		</div>					
		<div class="clearfix"></div>
		<div class="sent_time off">Sent On Tue, 2:45pm</div>
	</div>		
</div>
</div>
<div class="chat-input-wrapper" style="display:none">
	<textarea id="chat-message-input" rows="1" placeholder="Type your message"></textarea>
</div>
        