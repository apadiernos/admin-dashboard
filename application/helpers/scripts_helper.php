<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This file is used to provision js and css scripts
 *
 */
 
 function provision_scripts($script_files,$ext='css'){
	 $scripts = "";
	 foreach($script_files as $dir => $files){
		 foreach($files as $file){
			 switch($ext){
				case "css": 
					$scripts .= "\n".'<link href="'.base_url().'/webarch/assets/'.$dir.$file.'.'.$ext.'" rel="stylesheet" type="text/css"/>';
					break;
				default:
					$scripts .= "\n".'<script src="'.base_url().'/webarch/assets/'.$dir.$file.'.'.$ext.'"></script>';
					break;
			 }
		 }
	 }
	 echo $scripts;
 }
 
function get_list_scripts(){
	$params['js'] = array(
		'plugins/bootstrap-select2/' => array('select2.min'),
		'plugins/jquery-datatable/js/' => array('jquery.dataTables.min'),
		'plugins/jquery-datatable/extra/js/' => array('dataTables.tableTools.min'),
		'plugins/datatables-responsive/js/' => array('datatables.responsive','lodash.min'),
		'js/' => array('list'),
	);
	$params['css'] = array(
		'plugins/bootstrap-select2/' => array('select2'),
		'plugins/jquery-datatable/css/' => array('jquery.dataTables'),
		'plugins/datatables-responsive/css/' => array('datatables.responsive'),
	);
	return $params;	
}

function get_form_scripts(){
	$params['js'] = array(
		'plugins/bootstrap-wysihtml5/' => array('wysihtml5-0.3.0','bootstrap-wysihtml5'),
		'plugins/dropzone/' => array('dropzone.min'),
		'plugins/bootstrap-select2/' => array('select2.min'),
		'plugins/bootstrap-datepicker/js/' => array('bootstrap-datepicker'),
		'plugins/bootstrap-timepicker/js/' => array('bootstrap-timepicker.min'),
		'js/' => array('form_elements','form_validations'),
	);
	$params['css'] = array(
		'plugins/bootstrap-wysihtml5/' => array('bootstrap-wysihtml5'),
		'plugins/dropzone/css/' => array('dropzone'),
		'plugins/bootstrap-datepicker/css/' => array('datepicker'),
		'plugins/bootstrap-timepicker/css/' => array('bootstrap-timepicker'),
		'plugins/bootstrap-select2/' => array('select2'),
		'css/' => array('form_validations'),
	);
	return $params;
}