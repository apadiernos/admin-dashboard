<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	
/**
 * @params int $time unix timestamp
 * @params boolean $withtime if includes time
 * @description format a given timestamp
 * @return returns formatted timestamp
 */		
function format_time($timestamp,$withtime=false)
{
	$default_date_format = "m/d/Y";
	$default_time_format = "g:i A";	
	
	$format = $withtime ? " ".$default_time_format  : null;
	return date($default_date_format.$format,$timestamp);		
}	

function array_kshift(&$arr)
{
  list($k) = array_keys($arr);
  $r  = array($k=>$arr[$k]);
  unset($arr[$k]);
  return $r;
}

function getJson($jsonFile=null)
{
   $jsonDir = FCPATH.'webarch/assets/json/';
   $data = file_get_contents($jsonDir.$jsonFile.".json");
   return json_decode($data);
}

function getFileDirectory($config=array())
{
   $CI =& get_instance();
   if( $config['size'] ){
	   $basesize = $config['size'];
	   $CI->config->load('images');
	   if( $size = $CI->config->item("image_sizes")[$basesize] )
		   $size = "-".$size[0]."x".$size[1];
   }
	$filedatecreated = $config['file_timecreated'];
	if($filedatecreated){
		$dir = base_url('/uploads/'.date("Y",$filedatecreated).'/'.date("m",$filedatecreated));
		return $dir.'/'.$config['file_raw_name'].$size.$config['file_ext'];
	}
}