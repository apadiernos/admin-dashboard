<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This file is used to format messages in a standard way throughout the site
 *
 * @example $this->load->helper('message');
 *
 * Then simple echo show_messages() where required.
 */

function set_messages($type = IS_ERROR, $message = '')
{
	$CI =& get_instance();
	$CI->session->set_userdata('messagetype', $type);
	$CI->session->set_userdata('message', $message);
}

function show_messages()
{
    $CI =& get_instance(); 

    $messagetype = $CI->session->userdata('messagetype');
    $message = $CI->session->userdata('message');
    $result = "";
    
	if(isset($messagetype)){
		if ($messagetype == IS_ERROR) {
			$result.='
				<div class="alert alert-error">
                  <button class="close" data-dismiss="alert"></button>
				  <i class="fa fa-fa-exclamation"></i>
                  '.$message.'
                </div>';
		}else if($messagetype == IS_SUCCESS){
			$result.='
				<div class="alert alert-success">
                  <button class="close" data-dismiss="alert"></button>
				  <i class="fa fa-check"></i>
                  '.$message.'
                </div>';
		}
	}
    
	clear_messages();
	
    return $result;
}

function clear_messages()
{
	$CI =& get_instance();
	
	$CI->session->unset_userdata('messagetype');
	$CI->session->unset_userdata('message');
}


function setHeaderHome()
{
	$CI =& get_instance();
	
	$CI->session->set_userdata('nav_active', 'home');
}

function setHeaderCompanyInfo()
{
	$CI =& get_instance();
	
	$CI->session->set_userdata('nav_active', 'company_info');
}

function setHeaderUsers()
{
	$CI =& get_instance();
	
	$CI->session->set_userdata('nav_active', 'users');
}

function setHeaderMenu()
{
	$CI =& get_instance();
	
	$CI->session->set_userdata('nav_active', 'menu');
}

function setHeaderMediaObject()
{
	$CI =& get_instance();
	
	$CI->session->set_userdata('nav_active', 'media_object');

}

function getContentByName($menu_name = "")
{
	$CI =& get_instance();
	
	$CI->load->model( array('mmenu') );
	
	$get_content_info = $CI->mmenu->get_content_info_name($menu_name);
	
	return html_entity_decode($get_content_info->full_description, ENT_QUOTES) ;
}

function getAllMediaObjects($media_gallery_name = "")
{
	$CI =& get_instance();
	
	$CI->load->model( array('mgallery') );
	
	return $CI->mgallery->getAllMediaObjects($media_gallery_name);
}

?>
